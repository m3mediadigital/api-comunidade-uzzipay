<?php
/*Services*/
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*Controller*/
use App\Http\Controllers\PagesController;
use App\Http\Controllers\HomeController;
/*Master*/
use App\Http\Controllers\m3\UsersController;
use App\Http\Controllers\m3\CategorysController;
use App\Http\Controllers\m3\CommentController;
use App\Http\Controllers\m3\PagesController as M3PagesController;
use App\Http\Controllers\m3\PostsController;
/*User*/
use App\Http\Controllers\user\UserController;
use App\Http\Controllers\user\PostController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PagesController::class, 'index'])->name('index');

Auth::routes(['verify' => true]);

Route::get('/home',[HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth','verified']], function () {
	
	//User
	Route::resource('conta', UserController::class);
	Route::resource('postagens', PostController::class);

	//Master
	Route::resource('categories', CategorysController::class);
	Route::resource('users', UsersController::class)->except([
		'create'
	]);
	Route::resource('posts', PostsController::class)->except([
		'create','store'
	]);
	Route::resource('comentarios', CommentController::class)->except([
		'index','create','edit',
	]);
	Route::resource('pages', M3PagesController::class)->except([
		'create','show','store','destroy'
	]);

	//Site
	Route::get('categoria/{slug}', [PagesController::class, 'posts'])->name('posts');
	Route::get('post/{slug}', [PagesController::class, 'post'])->name('post');
	Route::get('user/{slug}', [PagesController::class, 'user'])->name('user');
	Route::post('comments', [PagesController::class, 'comments'])->name('comments');
	Route::get('curti/{id}', [PagesController::class, 'curti'])->name('curti');
	
	
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\m3\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\m3\ProfileController@update']);
	Route::get('upgrade', function () {return view('pages.upgrade');})->name('upgrade'); 
	Route::get('map', function () {return view('pages.maps');})->name('map');
	Route::get('icons', function () {return view('pages.icons');})->name('icons'); 
	Route::get('table-list', function () {return view('pages.tables');})->name('table');
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\m3\ProfileController@password']);

	//Pages Statics
	Route::get('{slug}', [PagesController::class, 'pages'])->name('pages');	
});

