<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\ApiController;
use App\Http\Controllers\api\UsersController;
use App\Http\Controllers\api\PostsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/',     [ApiController::class , 'index'])->name('index');
Route::get('categorys',     [ApiController::class , 'Categorys'])->name('Categorys');
Route::middleware('auth:sanctum')->get('categorys-posts/{slug}',     [ApiController::class , 'CategorysPosts'])->name('CategorysPosts');
Route::get('post/{slug?}',     [ApiController::class , 'post'])->name('post');
Route::post('users/create',     [UsersController::class , 'store'])->name('userstore');

//CREATE POSTS
Route::get('post-categorys', [PostsController::class, 'categorys'])->name('post-categorys');
Route::post('post-store', [PostsController::class, 'store'])->name('post-store');








Route::get('settings',  [ApiController::class , 'settings'])->name('settings');