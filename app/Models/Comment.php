<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
class Comment extends Model
{
    use HasFactory, Notifiable;

    protected $table = "comments";
    protected $fillable = ['user_id', 'post_id', 'content'];

    public function user()
    {
        return $this->belongsTo('App\Models\User')->select('file_id','name');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }

//     public function hasLike()
//     {
//         return $this->hasMany('App\Models\LikesHasComments', 'comment_id');
//     }
}
