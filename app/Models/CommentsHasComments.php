<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CommentsHasComments extends Model
{
    use HasFactory;
    
    protected $table = "comments_has_comments";
}
