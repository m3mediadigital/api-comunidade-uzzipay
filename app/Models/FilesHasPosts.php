<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FilesHasPosts extends Model
{ 
    use HasFactory;

    protected $table = "file_has_posts";

    public function file()
    {
        return $this->belongsTo('App\Models\File');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
