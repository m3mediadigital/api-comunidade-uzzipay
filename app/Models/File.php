<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class File extends Model
{
    use HasFactory;
    
    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function Upload(&$file, $base64, $path)
    {
        // dd($base64);
        if ($base64) {

            $contents = file_get_contents($base64);
            // dd($base64)
            $mimeType = Image::make($contents)->mime();
            $directory = $path;

            switch ($mimeType) {
                case 'image/jpeg':
                case 'image/jpg':
                    $extension = '.jpg';
                    break;

                case 'image/png':
                    $extension = '.png';
                    break;

                case 'image/bmp';
                    $extension = '.bmp';
                    break;

                case 'image/gif':
                    $extension = '.gif';
                    break;

                case 'image/tiff':
                case 'image/x-tiff':
                    $extension = '.tiff';
                    break;

                default:
                    throw new \Exception("Invalid Mime-Type file.");
            }

            $imageName = md5(microtime()) . $extension;

            if (!Storage::directories($directory)) {
                try {
                    Storage::makeDirectory($directory);
                } catch (\Throwable $th) {
                    throw $th;
                }
                
            }
            
            $path = public_path('storage/' . $directory . '/' . $imageName);

            try {
                Image::make($contents)->save($path);
                
                $file->name = $imageName;
                $file->path = $directory . '/' . $imageName;
                $file->type = "file";
                $file->save();

                return true;
            } catch (\Throwable $th) {
                throw $th;
            }

            return  false;
        }
    }
}
