<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use HasFactory, Sluggable;
    
    protected $fillable = ['title', 'user_id'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function post()
    {
        return $this->hasMany('App\Models\Post');
    }
    
    // private static function indexPost(&$query)
    // {
    //     $query = self::with([
    //         'post' => function($q){
    //             $q->select(
    //                 'id',
    //                 'category_id',
    //                 'title',
    //                 'content',
    //                 'active',
    //                 'slug'
    //             )->with([
    //                 'hasFile' => function($q){
    //                     $q->select(
    //                         'id',
    //                         'post_id',
    //                         'file_id'
    //                     )->with([
    //                         'file' => function($q){
    //                             $q->select(
    //                                 'id',
    //                                 'type',
    //                                 DB::raw('CONCAT("'.env('APP_URL').'/storage/", path) as image')
    //                             );
    //                         }
    //                     ]);
    //                 }
    //             ])->where('active',1);
    //         }
    //     ]);
    // }

    public static function defaultModel()
    {
        $query = self::query()
            ->select(
                'id',
                'title',
                'slug'
            );

        return $query;
    }
    public static function indexCategorys()
    {
        $query = self::defaultModel();
        $query->with([
                'post' => function($q){
                    $q->select(
                        'id',
                        'category_id',
                        'title',
                        'link',
                        'active',
                        'slug',
                        'updated_at'
                    )->where('active',1);
                }
            ]);

        $query = $query->limit(15)->inRandomOrder()->get();
    
        $items = [];

        foreach($query as $item){
            $item->post->count() > 0 ? $items[] = $item : '';
        }

        return $items;
    }

    public static function categoryPosts($slug)
    {   
        $query = self::defaultModel();
        $query->with([
            'post' => function ($q){
                $q->select(
                    'id',
                    'title',
                    'content',
                    'link',
                    'category_id',
                    'slug',
                    'user_id',
                    'updated_at'
                )->with([
                    'user'  => function($q){
                        $q->select(
                            'id',
                            'name',
                            'file_id',
                            'slug'
                        )->with([
                            'file' => function($q){
                                $q->select(
                                    'id',
                                    'path'
                                );
                            }
                        ]);
                    }
                ]);
            }
        ]);

        return $query->where('slug',$slug)->first();
    }

    public static function allCategory()
    {
        $query = self::defaultModel();
        return $query->orderBy('title','asc')->get();
    }
     
}
