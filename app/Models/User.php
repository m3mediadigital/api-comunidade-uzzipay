<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use GoldSpecDigital\LaravelEloquentUUID\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Cviebrock\EloquentSluggable\Sluggable;

use App\Models\Role;
class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens, Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'cpf',
        'password',
        'facebook',
        'instagram'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sluggable():array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }


    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function file()
    {
        return $this->belongsTo('App\Models\File');
    }

    public function post()
    {
        return $this->hasMany('App\Models\Post');
    }

    public function setData(&$user, $request)
    {
        $user->name = $request['name'] ?? $user->name;
        $user->cpf = $request['cpf'] ?? $user->cpf;
        $user->email = $request['email'] ?? $user->email;
        $user->facebook = $request['facebook'] ?? $user->facebook;
        $user->instagram = $request['instagram'] ?? $user->instagram;
        $user->about = $request['about'] ?? $user->about;
        $user->password = isset($request['password']) ? Hash::make($request['password']) : $user->password;
    }
}
