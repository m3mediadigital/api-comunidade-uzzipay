<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class FeaturedPost extends Model
{
    use HasFactory;

    protected $table = "featured_posts";
    protected $fillable = ['post_id'];

   
    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
