<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    use HasFactory, Sluggable, Notifiable;

    protected $table = "posts";

    protected $fillable = [
        'user_id',
        'active',
        'title',
        'content'
    ];

    public function sluggable():array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function hasFile()
    {
        return $this->hasOne('App\Models\FilesHasPosts','post_id','id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }

    public function like()
    {
        return $this->hasMany('App\Models\Like');
    }

    public function comment()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function featured()
    {
        return $this->belongsTo('App\Models\FeaturedPost');
    }

    public function setData($post, $request)
    {
        $post->title = $request['title'] ?? $post->title;
        $post->category_id = $request['category_id'] ?? $post->category_id;
        $post->content = $request['content'] ?? $post->content;
        $post->link = $request['link'] ?? $post->link ?? null;
    }

    public static function defaulModel()
    {
        $query = self::query()
            ->select(
                'id',
                'title',
                'content',
                'link',
                'slug',
                'active',
                'user_id',
                'category_id',
                'updated_at'
            )->with([
                'hasFile' => function($q){
                    $q->select(
                        'post_id',
                        'file_id',
                    )->with([
                        'file' => function($q){
                            $q->select(
                                'id',
                                DB::raw('CONCAT("'.env('APP_URL').'/storage/", path) as path')
                            );
                        }
                    ]);
                }
            ]);
        
        return $query;
    }

    public static function getSlug($slug)
    {
        $query = self::defaulModel();
        $query->with([
            'user' => function($q){
                $q->select(
                    'id',
                    'name',
                    'slug',
                    'file_id',
                )->with([
                    'file' => function($q){
                        $q->select(
                            'id',
                            'path'
                        );
                    }
                ]);
            },
            'category' => function ($q){
                $q->select(
                    'id',
                    'title',
                )->with([
                    'post' => function ($q){
                        $q->select(
                            'id',
                            'title',
                            'content',
                            'category_id',
                            'slug',
                            'user_id',
                            'updated_at'
                        )->with([
                            'user'  => function($q){
                                $q->select(
                                    'id',
                                    'name',
                                    'file_id'
                                )->with([
                                    'file' => function($q){
                                        $q->select(
                                            'id',
                                            DB::raw('CONCAT("'.env('APP_URL').'", path) as path')
                                        );
                                    }
                                ]);
                            }
                        ])->limit(5);
                    }
                ]);
            },
        ]);

        return $query->where([['slug', $slug]])->first();
    }
}
