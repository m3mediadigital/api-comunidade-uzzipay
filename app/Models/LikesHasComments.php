<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class LikesHasComments extends Model
{
    use HasFactory;
    
    protected $table = "likes_has_comments";
}
