<?php
namespace App\Helpers;

use App\Models\Log as ModelsLog;
use Illuminate\Support\Facades\Log;


class Logger
{

    public function log($level, $message, $id, $key, $after)
    {
        $message = '['.auth()->user()->name.'] - '. $message . json_encode($after);        

        $log = Log::channel('main')->$level($message);

        $log = new ModelsLog();
        $log->user_id = auth()->user()->id;
        $log->message = $message;
        $log->level = $level;
        $log->after = json_encode($after);
        $log->data = json_encode($log);
        $log->save();    
    }
}