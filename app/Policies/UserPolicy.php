<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    
    public function viewAny(User $user)
    {
        return in_array($user->role->name, ['Admin', 'Master']);
    }

    public function view(User $user, User $model)
    {
        return $this->viewAny($user);
    }

    public function create(User $user)
    {
        return $this->viewAny($user);
    }

    public function update(User $user, User $model)
    {
        if(!$this->viewAny($user))
            return $user->id === $model->id ? true : false;
        
        return $this->viewAny($user);
    }

    public function delete(User $user, User $model)
    {
        return $this->viewAny($user);
    }

    public function restore(User $user, User $model)
    {
        return $this->viewAny($user);
    }

    public function forceDelete(User $user, User $model)
    {
        return $this->viewAny($user);
    }
}
