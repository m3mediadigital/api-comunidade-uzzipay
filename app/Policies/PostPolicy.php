<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Policies\BasePolicyTrait;

class PostPolicy
{
    use HandlesAuthorization, BasePolicyTrait;

    
    public function viewAny(User $user)
    {
        return in_array($user->role->name, ['Admin', 'Master']);
    }
  
    public function view(User $user, Post $post)
    {
        return in_array($user->role->name, ['Admin', 'Master']);
    }

    public function create(User $user)
    {
        return $user;
    }

    public function update(User $user, Post $post)
    {
        if(!$this->viewAny($user))
            return $user->id === $post->user_id;

        return $this->viewAny($user);
    }

    public function delete(User $user, Post $post)
    {
        return $this->update($user, $post);
    }

    public function restore(User $user, Post $post)
    {
        return $this->viewAny($user);
    }

    public function forceDelete(User $user, Post $post)
    {
        return $this->viewAny($user);
    }
}
