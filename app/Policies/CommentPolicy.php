<?php

namespace App\Policies;

use App\Models\Comment;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return in_array($user->role->name, ['Admin', 'Master']);
    }

    public function view(User $user, Comment $comment)
    {
        return $this->viewAny($user);
    }

    public function create(User $user)
    {
        return $this->viewAny($user);
    }

    public function update(User $user, Comment $comment)
    {
        return $this->viewAny($user);
    }

    public function delete(User $user, Comment $comment)
    {
        return $this->viewAny($user);
    }

    public function restore(User $user, Comment $comment)
    {
        return $this->viewAny($user);
    }

    public function forceDelete(User $user, Comment $comment)
    {
        return $this->viewAny($user);
    }
}
