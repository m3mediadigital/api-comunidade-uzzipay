<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Category;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Policies\BasePolicyTrait;

class CategoryPolicy
{
    use HandlesAuthorization, BasePolicyTrait;

    
    public function viewAny(User $user)
    {
        return in_array($user->role->name, ['Admin', 'Master']);
    }

    public function view(User $user, Category $category)
    {
       return in_array($user->role->name, ['Admin', 'Master']);
    }

    public function onlyMaster(User $user)
    {
       return $user->role->name == 'Master';
    }

    public function create(User $user)
    {
       return in_array($user->role->name, ['Admin', 'Master']);
    }

    public function update(User $user, Category $category)
    {
       return in_array($user->role->name, ['Admin', 'Master']);
    }

    public function delete(User $user, Category $category)
    {
       return in_array($user->role->name, ['Admin', 'Master']);
    }

}
