<?php

namespace App\Policies;

use App\Models\Page;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PagePolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return in_array($user->role->name, ['Admin', 'Master']);
    }

    
    public function view(User $user, Page $page)
    {
        return $this->viewAny($user);
    }

    
    public function create(User $user)
    {
        return $this->viewAny($user);
    }

 
    public function update(User $user, Page $page)
    {
        return $this->viewAny($user);
    }

  
    public function delete(User $user, Page $page)
    {
        return $this->viewAny($user);
    }

  
    public function restore(User $user, Page $page)
    {
        return $this->viewAny($user);
    }

  
    public function forceDelete(User $user, Page $page)
    {
        return $this->viewAny($user);
    }
}
