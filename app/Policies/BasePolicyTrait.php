<?php

namespace App\Policies;

use App\Models\Role;

trait BasePolicyTrait
{
    
    public function roleMaster($user)
    {
        $role = Role::where('name','Master')->first();
        return $user->role->id === $role->id ?? false;
    }

    public function roleAdmin($user)
    {
        $role = Role::where('name','Admin')->first();
        return $user->role->id === $role->id ?? false;
    }

    public function roleUser($user)
    {
        $role = Role::where('name','User')->first();
        return $user->role->id === $role->id ?? false;
    }   
}