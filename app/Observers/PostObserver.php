<?php

namespace App\Observers;

use App\Models\Post;
use App\Notifications\Posts\CreateNotification;
use App\Notifications\Posts\DeleteNotification;
use App\Notifications\Posts\UpdateNotification;

class PostObserver
{

    public function created(Post $post)
    {
        $post->notify(new CreateNotification());
    }

    public function updated(Post $post)
    {
        $post->notify(new UpdateNotification());
    }

    public function deleted(Post $post)
    {
        $post->notify(new DeleteNotification());
    }

    public function restored(Post $post)
    {
        //
    }

    public function forceDeleted(Post $post)
    {
        //
    }
}
