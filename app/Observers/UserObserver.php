<?php

namespace App\Observers;

use App\Models\User;
use App\Notifications\User\CreateNotification;
use App\Notifications\User\DeleteNotification;
use App\Notifications\User\UpdateNotification;

class UserObserver
{
    
    public function created(User $user)
    {
        $user->notify(new CreateNotification());
    }
    
    public function updated(User $user)
    {
        $user->notify(new UpdateNotification());
    }
    
    public function deleted(User $user)
    {
        $user->notify(new DeleteNotification());
    }

    public function restored(User $user)
    {
        //
    }
    
    public function forceDeleted(User $user)
    {
        //
    }
}
