<?php

namespace App\Observers;

use App\Models\Comment;
use App\Notifications\Comment\CreateNotification;
use App\Notifications\Comment\DeleteNotification;
use App\Notifications\Comment\UpdateNotification;

class CommentObserver
{
    
    public function created(Comment $comment)
    {
        $comment->notify(new CreateNotification());
    }

    public function updated(Comment $comment)
    {
        $comment->notify(new UpdateNotification());
    }

    public function deleted(Comment $comment)
    {
        $comment->notify(new DeleteNotification());
    }

    public function restored(Comment $comment)
    {
        //
    }

    public function forceDeleted(Comment $comment)
    {
        //
    }
}
