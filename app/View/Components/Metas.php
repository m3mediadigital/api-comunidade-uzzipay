<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Metas extends Component
{
    public $title;
    public $file;
    public $description;
    public $subject;

    public function __construct($title, $file, $description, $subject)
    {
        $this->title = $title;
        $this->file = $file;
        $this->subject = $description;
        $this->subject = $subject;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.metas');
    }
}
