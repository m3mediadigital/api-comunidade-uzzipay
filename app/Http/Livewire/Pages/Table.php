<?php

namespace App\Http\Livewire\Pages;

use App\Models\Page;
use Livewire\Component;
use Livewire\WithPagination;

class Table extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public function render()
    {
        return view('livewire.pages.table',['pages' => Page::orderBy('title','asc')->paginate(15)]);
    }
}
