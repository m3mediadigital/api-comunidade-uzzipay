<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;

class Posts extends Component
{
    public $posts;

    public function render()
    {
        return view('livewire.users.posts',['posts' => $this->posts]);
    }
}
