<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;
use App\Models\User;
use Livewire\WithPagination;

class Lists extends Component
{
    use WithPagination;

     public $search;

    protected $queryString = ['search'];
    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.users.lists',[
            'users' => User::where([
                ['email','!=',auth()->user()->email],
                ['name', 'like', '%'.$this->search.'%']
            ])->paginate(15)
        ]);
    }
}
