<?php

namespace App\Http\Livewire\Users;

use Livewire\Component;

class Cards extends Component
{
    public $data;

    public function cards($item)
    {
        return [
            [
                'title' => 'Produtos',
                'icon' => 'shop text-default',
                'count' => $item->products->count(),
                'route' => 'companies.products.show',
                'id' => $item->id,
            ],
            [
                'title' => 'Slides',
                'icon' => 'tv-2 text-warning',
                'count' => $item->slides->count()
            ],
            [
                'title' => 'Configurações',
                'icon' => 'settings-gear-65 text-danger',
                'count' => $item->settings->count()
            ],
            [
                'title' => 'Logs',
                'icon' => "cloud-download-95 text-primary",
                'count' => $item->users->logs->count()
            ]
        ];
    }

    public function render()
    {
        return view('livewire.cards',['cards' => $this->cards($this->data)]);
    }
}
