<?php

namespace App\Http\Livewire\Site;

use App\Models\Comment as ModelsComment;
use App\Models\Post;
use Livewire\Component;

class Comment extends Component
{
    public $post;

    public function render()
    {
        return view('livewire.site.comment', ['comments' => 
            ModelsComment::where([
                ['post_id', $this->post],
                ['active',1]
            ])->get()
            ]
        );
    }
}
