<?php

namespace App\Http\Livewire\Site;

use Livewire\Component;
use App\Models\Category;
use App\Models\Post;

class Categorys extends Component
{
    public function render()
    {
        return view('livewire.site.categorys',[
            'categorys' => Category::indexCategorys(), 
            'posts' => Post::where('active',1)->orderBy('updated_at', 'asc')->get()
            ]
        );
    }
}
