<?php

namespace App\Http\Livewire\Site;

use App\Models\Category;
use Livewire\Component;

class Header extends Component
{
    public function render()
    {
        return view('livewire.site.header',['categorys' => Category::indexCategorys()]);
    }
}
