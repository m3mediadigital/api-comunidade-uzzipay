<?php

namespace App\Http\Livewire\Site\Posts;

use Livewire\Component;

class Card extends Component
{
    public $items;

    public function render()
    {
        return view('livewire.site.posts.card',['items' => $this->items]);
    }
}
