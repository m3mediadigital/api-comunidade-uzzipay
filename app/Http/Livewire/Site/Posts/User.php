<?php

namespace App\Http\Livewire\Site\Posts;

use Livewire\Component;

class User extends Component
{
    public $user;
    public $date;
    public $post;

    public function render()
    {
        // dd($this->post->like->count());
        // dd($this->date->format('d-m-y'));
        return view('livewire.site.posts.user',['user' => $this->user,'date' => $this->date, 'post' => $this->post]);
    }
}
