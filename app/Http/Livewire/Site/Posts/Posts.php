<?php

namespace App\Http\Livewire\Site\Posts;

use Livewire\Component;

class Posts extends Component
{
    public $posts;
    
    public function render()
    {
        return view('livewire.site.posts.posts',['item' => $this->posts]);
    }
}
