<?php

namespace App\Http\Livewire\Layout;

use Livewire\Component;

class Navbar extends Component
{
    public $title;

    public function render()
    {
        return view('livewire.layout.navbar', ['title' => $this->title]);
    }
}
