<?php

namespace App\Http\Livewire\Comment;

use App\Models\Comment;
use Livewire\Component;

class Lists extends Component
{
    public $post;

    public function render()
    {
        return view('livewire.comment.lists',['comments' => Comment::where('post_id',$this->post)->paginate(15)]);
    }
}
