<?php

namespace App\Http\Livewire\Categorys;

use App\Models\Post;
use Livewire\Component;
use Livewire\WithPagination;

class Posts extends Component
{
    use WithPagination;
    public $categorys, $search;

    protected $queryString = ['search'];
    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.categorys.posts', [
            'posts' => Post::where(
                [['category_id', $this->categorys->id],
                ['title', 'like', '%'.$this->search.'%']
                ])->paginate(20)]);
    }
}
