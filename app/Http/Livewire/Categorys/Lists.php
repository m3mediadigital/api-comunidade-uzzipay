<?php

namespace App\Http\Livewire\Categorys;

use Livewire\Component;
use App\Models\Category;
use Livewire\WithPagination;

class Lists extends Component
{
    use WithPagination;

    public $search;

    protected $queryString = ['search'];
    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.categorys.lists',['categories' => Category::where('title', 'like', '%'.$this->search.'%')->orderBy('created_at', 'desc')->paginate(20)]);
    }
}
