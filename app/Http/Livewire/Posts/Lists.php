<?php

namespace App\Http\Livewire\Posts;

use Livewire\Component;
use App\Models\Post;
use Livewire\WithPagination;

class Lists extends Component
{
    use WithPagination;

    public $search;

    protected $queryString = ['search'];
    protected $paginationTheme = 'bootstrap';

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.posts.lists',[
            'posts' => Post::where('title', 'like', '%'.$this->search.'%')->orderBy('created_at', 'asc')->paginate(20)
        ]);
    }
}
