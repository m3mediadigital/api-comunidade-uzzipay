<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'cpf' => ['required'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        // $this->verifyUzzipay($data['cpf']);

        $user = new User;
        $type = Role::where('name','User')->first();

        $user->setData($user, $data);
        $user->role_id = $type->id;

        try {
            $user->save();
        } catch (\Throwable $th) {
            throw $th;
            return redirect()->back()->with('error', 'Erro ao realizar cadastro. Por favor tente novamente.');
        }

        return $user;
    }


    public function verifyUzzipay($request)
    {
        $_URL = 'https://openhml-api.uzzipay.com/accounts/v1';

        $data = [];
        $data['agencia'] = 1;
        $data['tipoPessoa'] = 'F';
        $data['inscricao'] = $request;

        $header = array();
        $header[] = 'Content-length: 0';
        $header[] = 'Content-type: application/json';
        $header[] = 'Authorization: Bearer b8c8929e-1c74-3bb3-9b78-0b2d18c8b0e';

        $data = http_build_query($data);
        $curl = curl_init($_URL);

        // dd($header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $xml= curl_exec($curl);
        curl_close($curl);
        
        dd($xml);

        $xml= simplexml_load_string($xml);
        echo $xml -> code;

        dd($xml);
    }
}
