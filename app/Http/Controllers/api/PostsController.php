<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use App\Models\File;
use App\Models\FilesHasPosts;


class PostsController extends Controller
{
    
    public function validated($request)
    {
        return Validator::make($request->all(), [
            'title' => ['required', 'min:3'],
            'category_id' => ['required', 'uuid'],
            'file' => ['required','image', 'max:300'],
            'content' => ['required'],
        ]);

    }


    public function categorys()
    {
        return response()->json(Category::allCategory());
    }


    public function store(Request $request)
    {
        if (self::validated($request)->fails()) 
            return response()->json(['error' => self::validated($request)->errors()->first()]);
        
        $post = new Post;
        $file = new File;
        $has = new FilesHasPosts;

        $post->setData($post, $request->all());

        try {
            $post->save();

            try {
                $file->upload($file, $request->file, 'teste');
            } catch (\Throwable $th) {
                throw $th;
                return response()->json(['error' => 'Erro ao cadastrar imagem. Por favor tente novamente.']);
            }

            $has->file_id = $file->id;
            $has->post_id = $post->id;

            try {
                
                $has->save();

            } catch (\Throwable $th) {
                throw $th;
                return response()->json(['error' => 'Erro ao cadastrar imagem. Por favor tente novamente.']);
            }
        } catch (\Throwable $th) {
            throw $th;
            return response()->json(['error' => 'Erro ao cadastrar Post. Por favor tente novamente.']);
        }

        return response()->json(['success' => 'Post cadastrado e em análise.']);

    }

}
