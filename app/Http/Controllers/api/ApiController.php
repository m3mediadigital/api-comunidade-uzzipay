<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use App\Models\Post;

class ApiController extends Controller
{
    public function Categorys()
    {    
        return response()->json(Category::indexCategorys());   
    }

    public function CategorysPosts()
    {   
        return response()->json(Category::categoryPosts(request()->slug));   
    }

    public function post()
    {
        return response()->json(Post::getSlug(request()->slug));
    }

    
}
