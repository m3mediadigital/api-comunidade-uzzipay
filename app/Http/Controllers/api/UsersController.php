<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class UsersController extends Controller
{

    public function validated($request)
    {
        return Validator::make($request->all(), [
            'name' => ['required', 'min:3'],
            'email' => ['required', 'email:rfc,dns'],
            'cpf' => ['required'],
            'password' => ['required', 'confirmed', 'min:6']
        ]);

    }

    public function store(Request $request)
    {
        if (self::validated($request)->fails()) 
            return response()->json(['error' => self::validated($request)->errors()->first()]);

        $user = new User;
        $user->setData($user, $request->all());

        try {
            $user->save();
        } catch (\Throwable $th) {
            // throw $th;
            return response()->json(['error' => 'Erro ao realizar cadastro. Por favor tente novamente.']);
        }

        return response()->json(['success' => 'Usuário Cadastrado com Sucesso!']);
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

  
    public function destroy($id)
    {
        //
    }
}
