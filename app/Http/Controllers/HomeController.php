<?php

namespace App\Http\Controllers;

use App\Models\Role;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $role = Role::find(auth()->user()->role_id);
        
        switch ($role->name) {
            case 'User':
                 return view('user.index');
                break;

            case 'Master':
                return view('m3.dashboard');
                break;

            case 'Admin':
                return view('m3.dashboard');
                break;
            
            default:
                # code...
                break;
        }
        
    }
}
