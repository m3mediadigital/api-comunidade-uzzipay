<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\File;
use App\Models\FilesHasPosts;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Models\FeaturedPost;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }
    
    public function index()
    {
        //
    }

    
    public function create()
    {
        $this->authorize('create', Post::class);
        return view('user.posts.create',['categorys' => Category::orderBy('title','asc')->get()]);
    }

    
    public function store(PostRequest $request)
    {
        $this->authorize('create', Post::class);
        
        $post = new Post;
        $file = new File;
        $has = new FilesHasPosts;

        $post->setData($post, $request->all());
        $post->user_id = auth()->user()->id;

        try {
            $post->save();

            FeaturedPost::create(['post_id' => $post->id]);

            try {
                $file->upload($file, $request->file, auth()->user()->id);
            } catch (\Throwable $th) {
                throw $th;
                return redirect()->back()->with('error', 'Erro ao cadastrar imagem. Por favor tente novamente.');
            }

            $has->file_id = $file->id;
            $has->post_id = $post->id;

            try {
                
                $has->save();

            } catch (\Throwable $th) {
                throw $th;
                return redirect()->back()->with('error', 'Erro ao cadastrar imagem. Por favor tente novamente.');
            }
        } catch (\Throwable $th) {
            throw $th;
            return redirect()->back()->with('error', 'Erro ao cadastrar Post. Por favor tente novamente.');
        }

        return redirect()->route('home')->with('success', 'Post cadastrado e em análise.');
    }

    
    public function show(Post $post)
    {
        //
    }

    
    public function edit($id)
    {
        $this->authorize('update', $this->post->find($id));
        // dd($this->post->find($id));
        return view('user.posts.edit',['post' => $this->post->find($id), 'categorys' => Category::orderBy('title','asc')->get()]);
    }

    
    public function update(PostRequest $request, $id)
    {
        $post = $this->post->find($id);
        $this->authorize('update', $post);

        $post->setData($post, $request->all());
        $post->user_id = auth()->user()->id;

        try {
            $post->save();

        } catch (\Throwable $th) {
            throw $th;
            return redirect()->back()->with('error', 'Erro ao atualizar Post. Por favor tente novamente.');
        }

        if($request->file):
            $file = new File;
            $has = new FilesHasPosts;

            try {
                $file->upload($file, $request->file, auth()->user()->id);
            } catch (\Throwable $th) {
                throw $th;
                return redirect()->back()->with('error', 'Erro ao atualizar imagem. Por favor tente novamente.');
            }

            $has->file_id = $file->id;
            $has->post_id = $post->id;

            try {
                
                $has->save();

            } catch (\Throwable $th) {
                throw $th;
                return redirect()->back()->with('error', 'Erro ao atualizar imagem. Por favor tente novamente.');
            }
        endif;

        return redirect()->route('home')->with('success', 'Post atualizado e em análise.');
    }

    
    public function destroy($id)
    {
        $post = $this->post->find($id);
        $this->authorize('delete', $post);

        // dd($post->comment);
        try {
            $post->hasFile ? Storage::disk('public')->delete($post->hasFile->file->path) : '';
            if($post->comment->count() >= 1){

                foreach($post->comment as $item):
                    $item->delete();
                endforeach;
            }

            $post->delete();
            
        } catch (\Throwable $th) {
            // throw $th;
            return redirect()->back()->with('error', 'Erro ao exluir imagem. Por favor tente novamente.');
        }

        return redirect()->route('home')->with('success', 'Post apagado com sucesso.');
    }
}
