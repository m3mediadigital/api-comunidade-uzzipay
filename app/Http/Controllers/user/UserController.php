<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        return view('user.index');
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        
    }

   
    public function show($id)
    {

    }

    
    public function edit($id)
    {
        $this->authorize('update', $this->user->find($id));
        return view('user.account.edit',['user' => $this->user->find($id)]);
    }

    
    public function update(Request $request, $id)
    {
        $user = $this->user->find($id);
        $this->authorize('update', $user);

        if($request->file):

            $validator = Validator::make($request->all(), [
                'file' => 'max:300',    
            ]);

            if ($validator->fails()) {
                return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $file = new File;

            try {
                $file->upload($file, $request->file, auth()->user()->id);
            } catch (\Throwable $th) {
                throw $th;
                return redirect()->back()->with('error', 'Erro ao cadastrar imagem. Por favor tente novamente.');
            }
            $user->file_id = $file->id;
        endif;

        $user->setData($user, $request->all());
        

        try {
            $user->save();
        } catch (\Throwable $th) {
            // throw $th;
            return redirect()->back()->with('error', 'Erro ao atualizar cadastro. Por favor tente novamente.');
        }



        return redirect()->route('home')->with('success', 'Dados atualizado com Sucesso!');
    }

    
    public function destroy(User $user)
    {
        //
    }
}
