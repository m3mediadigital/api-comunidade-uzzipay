<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Page::class, 'page');
    }
    
    public function index()
    {
        return view('m3.pages.index');
    }

    
    public function create()
    {
        return view('m3.pages.create');
    }

   
    public function store(Request $request)
    {
        try {
            Page::create($request->all());
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error','Erro ao cadastrar Página');
        }

        return redirect()->route('pages.index')->with('success','Página criada com sucesso.');
    }


    public function show(Page $page)
    {
        //
    }


    public function edit(Page $page)
    {
        return view('m3.pages.edit',['page' => $page]);
    }

    
    public function update(Request $request, Page $page)
    {
        try {
            $page->update($request->all());
        } catch (\Throwable $th) {
            // throw $th;
            return redirect()->back()->with('error','Erro ao atualizar Página');
        }

        return redirect()->route('pages.index')->with('success','Página atualizada com sucesso.');
    }


    public function destroy(Page $page)
    {
        //
    }
}
