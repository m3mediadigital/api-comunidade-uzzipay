<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\FeaturedPost;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Post::class, 'post');
    }

    public function index()
    {
        return view('m3.posts.index');
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

    
    public function show(Post $post)
    {
        return view('m3.comments.index',['post' => $post]);
    }

    
    public function edit(Post $post)
    {
        return view('m3.posts.edit', ['post' => $post, 'categorys' => Category::orderBy('title', 'asc')->get()]);
    }

    
    public function update(Request $request, Post $post)
    {
        try {
            $post->update($request->all());
            FeaturedPost::where('post_id',$post->id)->update(['active' => $request->featured_post, 'post_id' => $post->id]);
        } catch (\Throwable $th) {
            throw $th;
            return redirect()->back()->with('error', 'Erro ao atualizar post!');
        }

        return redirect()->route('posts.index')->with('success','Post atualizado com sucesso!');
    }

    
    public function destroy(Post $post)
    {
        try {
            $post->delete();
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error', 'Não foi possível apagar o post.');
        }

        return redirect()->back()->with('success', 'Post removido com sucesso!');
    }
}
