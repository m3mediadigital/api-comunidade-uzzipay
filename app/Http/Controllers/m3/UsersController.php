<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }
    
    public function index()
    {
        return view('m3.users.index');
    }

    
    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $type = Role::where('name','User')->first();
        $user = new User;

        $user->setData($user, $request->all());   
        $user->role_id = $type->id;

        try {
            $user->save();
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error', 'Não foi possível cadastrar a usuário.');
        }

        return redirect()->route('users.index')->with('success', 'Usuário cadastrado com sucesso!');
    }

    
    public function show(User $user)
    {
        return view('m3.users.show',['user' => $user]);
    }

    
    public function edit(User $user)
    {
        return auth()->user()->role->name === "User" ? 
            redirect()->route('conta.edit', $user) : 
                view('m3.users.edit',['user' => $user]);
    }

    
    public function update(Request $request, User $user)
    {
        $user->setData($user, $request->all());

        try {
            $user->save();
        } catch (\Throwable $th) {
            // throw $th;
            return redirect()->back()->with('error', 'Não foi possível atualizar a usuário.');
        }

        return redirect()->route('users.index')->with('success', 'Usuário atualizado com sucesso!');
    }

    
    public function destroy(User $user)
    {
        if(isset($user->post)):
            foreach($user->post as $item):
                $item->delete();
            endforeach;
        endif;

        try {
            $user->delete();
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error', 'Não foi possível apagar a usuário.');
        }

        return redirect()->route('users.index')->with('success', 'Usuário apagado com sucesso!');
    }
}
