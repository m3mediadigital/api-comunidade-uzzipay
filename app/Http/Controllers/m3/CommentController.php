<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Notifications\Comment\UpdateNotification;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Comment::class, 'comment');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $comment = Comment::find($id);
        return view('m3.comments.show',['comment' => $comment]);
    }

    public function edit(Comment $comment)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);
        $comment->content = $request->content;
        $comment->active = $request->active;

        try {
            $comment->save();
            $comment->notify(new UpdateNotification());
        } catch (\Throwable $th) {
            // throw $th;
            return redirect()->back()->with('error','Erro ao atualizar comentário');
        }

        return redirect()->route('posts.show',$comment->post)->with('success','Comentário atualizado com sucesso!');
    }

    public function destroy($id)
    {
        $comment = Comment::find($id);

        try {
            $comment->delete();
        } catch (\Throwable $th) {
            // throw $th;
            return redirect()->back()->with('error','Erro ao apagar comentário');
        }

        return redirect()->route('posts.show',$comment->post)->with('success','Comentário apagado com sucesso!');
    }
}
