<?php

namespace App\Http\Controllers\m3;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

use App\Http\Requests\CategoryRequest;

class CategorysController extends Controller
{
    
    public function index()
    {
        $this->authorize('viewAny', Category::class);
        return view('m3.categories.index');
    }
    
    public function create()
    {
        $this->authorize('create', Category::class);
        return view('m3.categories.create');
    }

    public function store(CategoryRequest $request)
    {
        $this->authorize('create', Category::class);

        $category = new Category($request->all());
        $category->user_id = auth()->user()->id;
        
        try {
            $category->save();
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error', 'Não foi possível salvar a categoria.');
        }
        
        return redirect()->route('categories.index')->with('success', 'Categoria inserida com sucesso!');
    }
 
    public function show(Category $category)
    {
        return view('m3.categories.show',[
            'category' => $category
        ]);
    }
   
    public function edit(Category $category)
    {
        return view('m3.categories.edit',['category' => $category]);
    }
  
    public function update(Request $request, Category $category)
    {
        $this->authorize('create', Category::class);
        
        try {
            $category->update($request->all());
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->back()->with('error', 'Não foi possível salvar a categoria.');
        }
        
        return redirect()->route('categories.index')->with('success', 'Categoria atualizado com sucesso!');
    }
    
    public function destroy(Category $category)
    {
        $this->authorize('delete', $category);

        if(empty($category->post))
            return redirect()->back()->with('error', 'Exite Postagens com essa Categoria');

        try {
             $category->delete();
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Não foi possível apagar a categoria.');
        }

        return redirect()->route('categories.index')->with('success', 'Categoria removido com sucesso!');
    }
}
