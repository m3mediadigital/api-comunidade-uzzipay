<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\FeaturedPost;
use App\Models\Like;
use App\Models\Page;
use App\Models\Post;
use App\Models\user;

class PagesController extends Controller
{
    public function index()
    {
        return view('site.index',['featured' => FeaturedPost::where('active',1)->inRandomOrder()->limit(3)->get()]);
    }

    public function posts($slug)
    {   
        return view('site.posts',['category' => Category::categoryPosts($slug)]);
    }

    public function post($slug)
    {
        return view('site.post',['post' => Post::getSlug($slug)]);
    }

    public function user($slug)
    {
        $user = User::where('slug',$slug)->first();    
        $user ?? abort(404, 'Usuário não encontrado');

        return view('site.user',['user' => $user]);
    }

    public function comments(Request $request)
    {
        $request['user_id'] = auth()->user()->id;
   
        try {
            Comment::create($request->all());
        } catch (\Throwable $th) {
            throw $th;
        }

        return redirect()->back()->with('success','Comentário cadastrado e em análise');
    }

    public function curti($id)
    {
        $like = Like::where([['user_id', auth()->user()->id],['post_id', $id]])->first();

        if(!$like):
            $like = new Like;
                $like->create([
                    'user_id' => auth()->user()->id,
                    'post_id' => $id
                ]);

                $like = Like::where([['user_id', auth()->user()->id],['post_id', $id]])->count();
            return response()->json(['status' => true, 'count' => $like]);
        else:
            $like->delete();
            $like = Like::where([['user_id', auth()->user()->id],['post_id', $id]])->count();
            return response()->json(['status' => false, 'count' => $like]);
        endif;
    }

    public function pages($slug)
    {
        return view('site.pages',['page' => Page::where('slug', $slug)->first()]);
    }
}
