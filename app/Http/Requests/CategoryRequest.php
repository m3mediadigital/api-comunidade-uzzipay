<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:255',
        ];

        return $rules;


    }

    public function messages()
    {
        return [
            'title.required' => 'O nome da categoria é obrigatório',
            'title.max' => 'O nome da categoria é muito longo!',
        ];
    }
}
