<?php

namespace Database\Factories;

use App\Models\LikesHasComments;
use App\Models\User;
use App\Models\CommentsHasComments;
use Illuminate\Database\Eloquent\Factories\Factory;

class LikesHasCommentsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = LikesHasComments::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'active' => $this->faker->boolean,
            'user_id' => User::factory(),
            'commenets_has_comments_id' => CommentsHasComments::factory(),
        ];
    }
}
