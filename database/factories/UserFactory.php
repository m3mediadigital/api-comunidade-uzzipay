<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\File;
use App\Models\TypeUser;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $type = TypeUser::where('name','Admin')->first();
        
        return [
            'active' => $this->faker->boolean,
            'file_id' => File::factory(),
            'type_user_id' => $type->id,
            'name' => $this->faker->name,
            'cpf' => '00000000000',
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'password' => Hash::make('123456'),
            'remember_token' => Str::random(100),
        ];
    }
}
