<?php

namespace Database\Factories;

use App\Models\CommentsHasComments;
use App\Models\User;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentHasCommentsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CommentsHasComments::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'active' => $this->faker->boolean,
            'user_id' => User::factory(),
            'commnent_id' => Comment::factory(),
            'post_id' => Post::factory(),
            'content' => $this->faker->text,
        ];
    }
}
