<?php

namespace Database\Factories;

use App\Models\FilesHasPosts;
use App\Models\Post;
use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;

class FilesHasPostsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FilesHasPosts::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'file_id' => File::factory(),
            'post_id' => Post::factory()
        ];
    }
}
