<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->uuid('role_id')->after('id');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->uuid('file_id')->after('id')->nullable();
            $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {     
            $table->dropForeign(['file_id']);            
            $table->dropForeign(['role_id']); 
            $table->dropColumn('file_id');
            $table->dropColumn('role_id');
        });
    }
}
