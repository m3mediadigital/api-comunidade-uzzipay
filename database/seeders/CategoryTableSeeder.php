<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\Category::factory(1)->create();

        $user = User::first();
        $items = [
            [
                'user_id'  => $user->id,
                'title'     => 'Categoria I',
            ],
            [
                'user_id'  => $user->id,
                'title'     => 'Categoria II',
            ],
        ];
        
        foreach($items as $key => $value):
            $category = new Category($value);
            $category->save();
        endforeach;
    }
}
