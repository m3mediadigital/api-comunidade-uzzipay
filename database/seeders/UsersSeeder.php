<?php

namespace Database\Seeders;

use App\Models\File;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = Role::where('name', 'User')->first();
        $master = Role::where('name', 'Master')->first();
        $items = [
            [
                'active'   => true,
                'cpf'      => '123.123.123-00',
                'name'     => 'Usuário de Teste',
                'email'    => 'teste@teste.com',
                'password' => Hash::make('123123'),
                'role_id'  => $type->id,
                'file' => ['name' => 'avatar-woman.svg', 'path' => '/images/min/avatar-woman.svg'],
            ],
            [
                'active'   => true,
                'cpf'      => '000.000.000-00',
                'name'     => 'NovaM3',
                'email'    => 'dev@novam3.com',
                'password' => Hash::make('123123'),
                'role_id'  => $master->id,
                'file' => ['name' => 'avatar-woman.svg', 'path' => '/images/min/avatar-woman.svg'],
            ],
        ];
        
        foreach($items as $key => $value):
            $file = new File($value['file']);
            unset($value['file']);
            $file->save();

            $user = new User($value);
            $user->file_id = $file->id;
            $user->save();
        endforeach;
    }
}

