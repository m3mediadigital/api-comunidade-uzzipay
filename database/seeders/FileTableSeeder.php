<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\File;

class FileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(File::class, 50)->create();
        // \App\Models\File::factory()->count(30)->create(); 
       File::factory(1)->create();
    }
}
