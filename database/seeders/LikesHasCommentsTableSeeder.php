<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LikesHasCommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\LikesHasComments::factory(3000)->create();
    }
}
