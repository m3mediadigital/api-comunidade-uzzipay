<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CommentHasCommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\CommentsHasComments::factory(40000)->create();
    }
}
