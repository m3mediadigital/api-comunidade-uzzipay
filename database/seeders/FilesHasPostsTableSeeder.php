<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FilesHasPostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\FilesHasPosts::factory(101)->create();
    }
}
