<?php

namespace Database\Seeders;

use App\Models\Page;
use App\Models\User;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email','dev@novam3.com')->first();
        $items = [
            [
                'user_id' => $user->id,
                'title' => 'Termos de uso',
                'content' => 'Termos de uso'
            ],
            [
                'user_id' => $user->id,
                'title' => 'Sobre',
                'content' => 'Sobre'
            ],
            [
                'user_id' => $user->id,
                'title' => 'Privacidade',
                'content' => 'Privacidade'
            ]
        ];

        foreach($items as $item):
            Page::create($item);
        endforeach;
    }
}
