<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call([FileTableSeeder::class]);
 
        $this->call([
            // FileTableSeeder::class,
            RolesTableSeeder::class,
            UsersSeeder::class,
            CategoryTableSeeder::class,
            PagesTableSeeder::class,
            // PostTableSeeder::class,
            // FilesHasPostsTableSeeder::class,
            // CommentTableSeeder::class,
            // CommentHasCommentsTableSeeder::class,
            // LikeTableSeeder::class,
            // LikesHasCommentsTableSeeder::class,
        ]);
    }
}
