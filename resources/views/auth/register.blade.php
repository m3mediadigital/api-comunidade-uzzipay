<x-site>
    <x-slot name="title">
        {{ __('Cadastre-se') }}
    </x-slot>

    <x-slot name="content">
        <section class="login">
            <div class="container">
                <div class="row">
                    <div class="d-none d-lg-block col-lg-6 col-sm-12">
                        <div style="background-image:url(/images/postOne.png)"
                            class="w-100 border-0 posts-card posts-card-one card">
                            <div class="p-5 w-100 card-body">
                                <div class="card-body-content position-absolute pt-5">
                                    <div class="w-75 pb-3 pt-5 card-title h5"><strong>Comunidade</strong><br><strong
                                            class="text-green">Uzzipay!</strong></div>
                                    <p class="pb-3 card-text">A comunidade da Uzzipay é um espaço ideal para tirar
                                        dúvidas, dar sugestões e trocar ideias sobre sustentabilidade.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center align-items-center col-lg-6 col-sm-12">
                        <form class="login-forms login-forms-register w-100" method="POST"
                            action="{{ route('register') }}">
                            @csrf
                            <div class="w-100 pb-3">
                                <h1><strong>Cadastre-se</strong></h1>
                                <p>Faça o seu cadastro e interaja com nossos diversos <br> usuários!</p>
                            </div>
                            <div class="form-group w-100">
                                @error('name')
                                    <small id="emailHelp" class="form-text text-danger">
                                        {{ $message }}
                                    </small>
                                @enderror
                                <input type="text" class="form-control" placeholder="NOME" name="name"
                                    value="{{ old('name') }}">
                                {{-- <label for="login">Nome</label> --}}
                            </div>
                            <div class="form-group w-100">
                                @error('email')
                                    <small id="emailHelp" class="form-text text-danger">
                                        {{ $message }}
                                    </small>
                                @enderror
                                <input type="mail" class="form-control" placeholder="E-MAIL" name="email"
                                    value="{{ old('email') }}">
                                {{-- <label for="login">E-mail</label> --}}
                            </div>
                            <div class="form-group w-100">
                                @error('cpf')
                                    <small id="emailHelp" class="form-text text-danger">
                                        {{ $message }}
                                    </small>
                                @enderror
                                <input type="tel" class="form-control" placeholder="CPF" name="cpf"
                                    value="{{ old('cpf') }}">
                                {{-- <label for="login">CPF</label> --}}
                            </div>
                            <div class="form-row">
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group w-100">
                                        @error('password')
                                            <small id="emailHelp" class="form-text text-danger">
                                                {{ $message }}
                                            </small>
                                        @enderror
                                        <input type="password" class="form-control" placeholder="SENHA" name="password">
                                        {{-- <label for="password">Senha</label> --}}
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group w-100">
                                        @error('password_confirmation')
                                            <small id="emailHelp" class="form-text text-danger">
                                                {{ $message }}
                                            </small>
                                        @enderror
                                        <input type="password" class="form-control" placeholder="CONFIRME A SENHA"
                                            name="password_confirmation">
                                        {{-- <label for="password">Confirme a Senha</label> --}}
                                    </div>
                                </div>
                                <div class="pt-5 col-md-6 col-sm-12 d-flex align-items-center">
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <label class="form-check-label" for="exampleCheck1">
                                            Concordo com os termos e condições
                                        </label>
                                    </div>
                                </div>
                                <div class="pt-2 col-md-6 col-sm-12">
                                    <button class="btn btn-success text-uppercase w-100 mt-4 mb-4 p-2" type="submit">
                                        Cadastrar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
</x-site>
