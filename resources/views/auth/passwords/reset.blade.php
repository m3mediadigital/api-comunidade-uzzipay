<x-site>
    <x-slot name="title">
        {{ __('Redefinir Senha') }}
    </x-slot>

    <x-slot name="content">
        <section class="login">
            <div class="container">
                <div class="row">
                    <div class="d-none d-lg-block col-lg-6 col-sm-12">
                        <div style="background-image:url(/images/postOne.png)"
                            class="w-100 border-0 posts-card posts-card-one card">
                            <div class="p-5 w-100 card-body">
                                <div class="card-body-content position-absolute pt-5">
                                    <div class="w-75 pb-3 pt-5 card-title h5"><strong>Comunidade</strong><br><strong
                                            class="text-green">Uzzipay!</strong></div>
                                    <p class="pb-3 card-text">A comunidade da Uzzipay é um espaço ideal para tirar
                                        dúvidas, dar sugestões e trocar ideias sobre sustentabilidade.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center align-items-center col-lg-6 col-sm-12">
                        <form class="login-forms" method="POST" action="{{ route('password.update') }}">
                            @csrf

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="w-100 pb-3 pt-3">
                                <h1 class="text-center">Redefinir <strong>Senha</strong></h1>
                            </div>
                            <div class="form-group w-100 mb-3">
                                @error('email')
                                    <small id="emailHelp" class="form-text text-danger">
                                        {{ $message }}
                                    </small>
                                @enderror
                                <input type="text" class="form-control" placeholder=" " name="email"
                                    value="{{ $email ?? old('email') }}">
                                <label for="login">E-mail</label>
                            </div>
                            <div class="form-group w-100 mb-3">
                                @error('password')
                                    <small id="emailHelp" class="form-text text-danger">
                                        {{ $message }}
                                    </small>
                                @enderror
                                <input type="password" class="form-control" placeholder=" " name="password">
                                <label for="password">Senha</label>
                            </div>
                            <div class="form-group w-100 mb-3">
                                <input type="password" class="form-control" placeholder=" "
                                    name="password_confirmation">
                                <label for="password">Confirme Senha</label>
                            </div>
                            <button class="btn btn-success w-100 mt-4 mb-4 p-2" type="submit">
                                Redefinir Senha
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
</x-site>
