<x-site>
    <x-slot name="title">
        {{ __('Preferências') }}
    </x-slot>

    <x-slot name="css">
        <style type="text/css">
            .btn-danger .file-upload {
                width: 100%;
                padding: 10px 0px;
                position: absolute;
                left: 0;
                opacity: 0;
                cursor: pointer;
            }

        </style>
    </x-slot>

    <x-slot name="content">
        <section class="container">
            <div class="pt-5 pb-4">
                <h3>
                    <strong>Preferências</strong>
                </h3>
            </div>
            <form class="admin-form" method="POST" enctype="multipart/form-data"
                action="{{ route('conta.update', auth()->user()->id) }}">
                @csrf
                @method('put')
                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="">Nome <em class="text-danger">*</em></label>
                        <input type="text" name="name"
                            class="form-control rounded-0 @error('name') is-invalid @enderror"
                            value="{{ $user->name }}">
                        @error('name')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="">E-mail <em class="text-danger">*</em></label>
                        <input type="email" name="email"
                            class="form-control rounded-0 @error('email') is-invalid @enderror"
                            value="{{ $user->email }}">
                        @error('email')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                    <div class="form-group col-sm-12 col-md-4">
                        <label for="">Usuário <em class="text-danger">*</em></label>
                        <input type="text" name="text"
                            class="form-control rounded-0 @error('name') is-invalid @enderror"
                            value="{{ $user->name }}" disabled>
                        @error('name')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                    <div class="form-group col-sm-12 col-md-4">
                        <label for="">Senha</label>
                        <input type="password" name="password"
                            class="form-control rounded-0 @error('password') is-invalid @enderror">
                        @error('password')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                    <div class="form-group col-sm-12 col-md-4">
                        <label for="">Confirmar Senha</label>
                        <input type="password" name="password_confirmation"
                            class="form-control rounded-0 @error('password_confirmation') is-invalid @enderror">
                        @error('password_confirmation')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4 admin" style="background-color: transparent">
                        <div class="d-flex justify-content-center align-items-center flex-column">
                            <img src="{{ asset(auth()->user()->file ? Storage::url(auth()->user()->file->path) : '/images/min/avatar-woman.svg') }}"
                                class="avatar" id="imagem">
                            <div class="btn btn-sm btn-danger">
                                <input type="file" class="file-upload" id="file-upload" name="file" accept="image/*">
                                Atualizar imagem
                            </div>
                        </div>
                        @error('file')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                    <div class="form-group col-sm-12 col-md-8">
                        <label>Sobre</label>
                        <textarea name="about"
                            class="form-control h-auto rounded-0 @error('about') is-invalid @enderror"
                            rows="7">{{ $user->about ?? old('about') }}</textarea>
                        @error('about')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <label for="">Facebook</label>
                        <input type="text" name="facebook"
                            class="form-control rounded-0 @error('facebook') is-invalid @enderror"
                            value="{{ $user->facebook }}">
                        @error('facebook')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <label for="">Instagram</label>
                        <input type="text" name="facebook"
                            class="form-control rounded-0 @error('instagram') is-invalid @enderror"
                            value="{{ $user->instagram }}">
                        @error('instagram')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                </div>
                <div class="text-right login">
                    <button type="submit" class="btn btn-success">atualizar</button>
                </div>
            </form>
        </section>
    </x-slot>
    <x-slot name="js">
        <script type="text/javascript">
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader()
                    reader.onload = function(e) {
                        $('#imagem').attr('src', e.target.result)
                    }
                    reader.readAsDataURL(input.files[0])
                }
            }
            $('#file-upload').change(function() {
                readURL(this)
            })

        </script>
    </x-slot>
</x-site>
