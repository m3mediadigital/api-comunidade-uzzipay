<x-site>
    <x-slot name="title">
        {{ __('Adiconar Novo Post') }}
    </x-slot>

    <x-slot name="content">
        <section class="container">
            <div class="pt-5 pb-4">
                <h3>
                    <strong>Nova Postagem</strong>
                </h3>
            </div>
            <div class="hr pt-4 pb-4">
                <p>Todos os campo com <em class="text-danger">*</em> são obrigatórios!</p>
                <hr>
            </div>
            <form class="posts-form" method="POST" enctype="multipart/form-data"
                action="{{ route('postagens.store') }}">
                @csrf
                @method('post')
                <div class="form-row">
                    <div class="form-group col-sm-12">
                        <label for="">Título <em class="text-danger">*</em></label>
                        <input type="text" name="title" class="form-control @error('title') is-invalid @enderror"
                            value="{{ old('title') }}" placeholder="Título aqui">
                        @error('title')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="">Categoria <em class="text-danger">*</em></label>
                        <select name="category_id"
                            class=" rounded-0 form-control @error('category_id') is-invalid @enderror">
                            <option value="">Selecione Categoria</option>
                            @foreach ($categorys as $item)
                                <option value="{{ $item->id }}"
                                    {{ old('category_id') === $item->id ? 'selected' : '' }}>
                                    {{ $item->title }}
                                </option>
                            @endforeach
                        </select>
                        @error('category_id')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="file">Imagem <em class="text-danger">*</em></label>
                        <input type="file" name="file" class="form-control-file">
                        @error('file')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="link">YouTube (Post em video, colocar código do video do youtube) </label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">https://www.youtube.com/watch?v=</div>
                            </div>
                            <input type="text" name="link" class="form-control @error('link') is-invalid @enderror"
                                value="{{ old('link') }}">
                        </div>
                        @error('link')
                            <small id="emailHelp" class="form-text text-danger">
                                {{ $message }}
                            </small>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="content">Conteúdo <em class="text-danger">*</em></label>
                    <textarea name="content" id="content" cols="30" rows="10">{{ old('content') }}</textarea>
                    @error('content')
                        <small id="emailHelp" class="form-text text-danger">
                            {{ $message }}
                        </small>
                    @enderror
                </div>
                </div>
                <div class="text-right login">
                    <button type="submit" class="btn btn-success">Postar</button>
                </div>
            </form>
        </section>
        <x-slot name="js">
            <script src="https://cdn.ckeditor.com/4.16.0/basic/ckeditor.js"></script>
            <script>
                CKEDITOR.replace('content', {
                    height: 300
                });

            </script>
        </x-slot>
    </x-slot>
</x-site>
