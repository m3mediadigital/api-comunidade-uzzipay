<x-site>
    <x-slot name="title">
        {{ __('Bem Vindo') }}
    </x-slot>

    <x-slot name="content">
        <section class="admin">
            <div class="pt-5 pt-md-0 pr-lg-4 pl-lg-4 pt-lg-2 pb-lg-2 container">
                <div class="row">
                    <div class="d-flex justify-content-center align-items-center col-lg-3 col-md-3 col-sm-12">
                        <img src="{{ asset(auth()->user()->file ? Storage::url(auth()->user()->file->path) : 'images/min/avatar-woman.svg') }}"
                            class="avatar">
                    </div>
                    <div class="pt-5 pb-5 admin-content d-flex align-items-center justify-content-center justify-content-md-start 
                        text-center text-md-left col-lg-9 col-md-9 col-sm-12">
                        <div class="p-0 w-100 row">
                            <div class="col-lg-6 col-md-7 col-sm-12">
                                <h2 class="text-uppercase m-0">{{ auth()->user()->name }}</h2>
                                <p>{{ '@' . str_replace(' ', '', str_replace('-', '', auth()->user()->name)) }}</p>
                                {{-- <p class="text-uppercase">10 seguindo | 300 seguidores</p> --}}
                            </div>
                            <div class="d-flex justify-content-center justify-content-lg-end 
                                col-lg-6 col-md-5 col-sm-12">
                                <div class="flex-row align-items-center likes nav">
                                    <div class="d-flex align-content-center pl-0 pl-lg-2 nav-item">
                                        <a href="{{ route('conta.edit', auth()->user()->id) }}"
                                            class="d-flex p-2 nav-link">
                                            <div class="icons">
                                                <img src="{{ asset('images/icons/settings.svg') }}">
                                            </div>
                                        </a>
                                    </div>
                                    {{-- <div class="d-flex align-content-center pl-0 pl-lg-2 nav-item">
                                        <a href="./m3/preferencias" class="d-flex p-2 nav-link">
                                            <div class="icons">
                                                <img src="{{ asset('images/icons/messages.svg') }}">
                                            </div>
                                        </a>
                                    </div> --}}
                                    <div class="d-flex align-content-center pl-0 pl-lg-2 nav-item">
                                        <a href="{{ route('postagens.create') }}" class="d-flex p-2 nav-link">
                                            <div class="icons">
                                                <img src="{{ asset('images/icons/posts.svg') }}">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @isset(auth()->user()->about)
                                <div class="admin-content-about text-justify col-sm-12">
                                    <p><strong>SOBRE</strong></p>
                                    <p>{{ auth()->user()->about }}</p>
                                </div>
                            @endisset
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="posts pt-3">
            <div class="posts-list container">
                <div class="pt-4 pl-0 pb-4">
                    <h3><strong>Postagens</strong></h3>
                </div>
                <div class="row">
                    @forelse (auth()->user()->post()->where('active',1)->get() as $item)
                        <div class="pb-4 col-md-6 col-sm-12">
                            <div class="w-100 posts-card pt-4 pb-4 mb-4 border-0 card">
                                <div class="pl-md-5 pr-md-5 pl-2 pr-2 pl-lg-5 pr-lg-5 row">
                                    <div class="col-sm-12">
                                        <ul class="nav d-flex justify-content-between">
                                            <li class="nav-item">
                                                <div class="card-title h5">
                                                    <a href="{{ route('post', ['slug' => $item->slug]) }}" target="_blank"
                                                        rel="noopener noreferrer">
                                                        {{ $item->title }}
                                                    </a>
                                                </div>
                                            </li>
                                            <li class="nav-item">
                                                <div class="card-title h5">
                                                    <div class="btn-group dropleft">
                                                        <button type="button" class="border-0" title="Opções"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                            <img src="{{ asset('images/icons/menu-card.svg') }}"
                                                                width="17" alt="menu">
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item"
                                                                href="{{ route('postagens.edit', $item->id) }}">Editar</a>
                                                            @livewire('delete', ['route' => route('postagens.destroy',
                                                            $item->id)],
                                                            key($item->id))
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>

                                        <div class="card-text">
                                            {!! Str::limit($item->content, 250) !!}
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center justify-lg-content-end pt-4 col-sm-12">
                                        @livewire('site.posts.user', ['user' => auth()->user()], key($auth()->user()->id))
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="w-100 text-center pt-5 pb-5">
                            {{ auth()->user()->post->count() >= 1
    ? 'Sua(s) estar em análize!'
    : 'Você ainda não tem postagens ! :(' }}
                        </div>
                    @endforelse
                </div>
            </div>
        </section>
    </x-slot>
</x-site>
