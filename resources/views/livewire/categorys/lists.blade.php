<div class="card w-100">
    <!-- Card header -->
    <div class="card-header border-0">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h3 class="mb-0">Lista</h3>
            </div>
            <div class="col-sm-12 col-md-5">
                <div id="datatable-basic_filter" class="dataTables_filter">
                    <input wire:model="search" type="search" class="form-control form-control-sm"
                        placeholder="Pesquisar">
                </div>
            </div>
            <div class="col-sm-12 col-md-1 text-right">
                <button class="btn btn-primary btn-sm" type="button" data-toggle="collapse"
                    data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    Adicionar
                </button>
            </div>
            <div class="col-12">
                <div class="collapse pt-5{{ old('title') ? ' show' : '' }}" id="collapseExample">
                    <form class="" method="post" action="{{ route('categories.store') }}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-12 col-md-12 col-lg-10 col-xl-11">
                                <input type="text" class="form-control" id="inlineFormInputName2" name="title"
                                    placeholder="Categoria" value="{{ old('title') }}">
                            </div>
                            <div class="form-group col-12 col-md-12 col-lg-2 col-xl-1">
                                <button type="submit" class="btn btn-success mb-2 w-auto">Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
                <tr>
                    <th scope="col">Categoria</th>
                    <th scope="col">Criada por</th>
                    <th scope="col">Criado em</th>
                    <th scope="col">Atualizado em</th>
                    <th scope="col">Opções</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                    <tr>
                        <td>{{ $category->title }}</td>
                        <td>{{ $category->user->name }}</td>
                        <td>{{ $category->created_at->format('d-m-Y') }}</td>
                        <td>{{ $category->updated_at->format('d-m-Y') }}</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-sm text-black-50" href="#" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-list"></i> Opções
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item" href="{{ route('categories.show', $category) }}">
                                        <i class="fas fa-mail-bulk"></i>Posts</a>
                                    <a class="dropdown-item" href="{{ route('categories.edit', $category) }}">
                                        <i class="fas fa-edit"></i>Editar</a>
                                    @livewire('delete', ['route' => route('categories.destroy', $category)],
                                    key($category->id))
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer py-4">
        {{ $categories->links() }}
    </div>
</div>
