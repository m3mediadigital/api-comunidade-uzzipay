<div class="header-body w-100">
    <div class="row">
        @foreach ($cards as $item)
            <div class="col-xl-3 col-lg-6">
                <div class="card card-stats mb-4 mb-xl-0">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted mb-0">{{ $item['title'] }}</h5>
                                <span class="h2 font-weight-bold mb-0">{{ $item['count'] }}</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape text-white rounded-circle shadow">
                                    <i class="ni ni-{{ $item['icon'] }} "></i>
                                </div>
                            </div>
                        </div>
                        <p class="mt-3 mb-0 text-muted text-sm">
                            <i class="ni ni-{{ $item['icon'] }} "></i>
                            <a
                                href="{{ @$item['route'] ? ($item['id'] ? route($item['route'], @$item['id']) : route($item['route'])) : '#' }}">
                                <span class="text-nowrap">Lista</span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
