<div class="card w-100">
    <!-- Card header -->
    <div class="card-header border-0">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <h3 class="mb-0">Comentários</h3>
            </div>
            <div class="col-sm-12 col-md-4">
                <div id="datatable-basic_filter" class="dataTables_filter">
                    <input wire:model="search" type="search" class="form-control form-control-sm"
                        placeholder="Pesquisar">
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
                <tr>
                    <th scope="col">User</th>
                    <th scope="col">Commentário</th>
                    {{-- <th scope="col">Curtidas</th> --}}
                    <th scope="col">Ativo</th>
                    <th scope="col">Criado</th>
                    <th scope="col">Atualizado</th>
                    <th scope="col">Opções</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($comments as $item)
                    <tr>
                        <td>{{ $item->user->name }}</td>
                        <td> {{ Str::limit($item->content, 100) }}</td>
                        {{-- <td>{{ $item->hasLike->count() }}</td> --}}
                        <td>
                            <span class="badge badge-pill badge-{{ $item->active == 1 ? 'success' : 'danger' }}">
                                {{ $item->active == 1 ? 'Ativo' : 'Inativo' }}
                            </span>
                        </td>
                        <td>{{ $item->created_at->format('d-m-Y') }}</td>
                        <td>{{ $item->updated_at->format('d-m-Y') }}</td>
                        <td>
                            <a class="btn btn-sm text-black-50" href="{{ route('comentarios.show', $item->id) }}">
                                <i class="fas fa-list"></i> Opções
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="card-footer py-4">
        {{ $comments->links() }}
    </div>
</div>
