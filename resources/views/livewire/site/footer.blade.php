<footer class="pt-4 pt-lg-5 pb-3 position-absolute w-100">
    <div class="d-none d-lg-block container">
        <nav class="navbar navbar-expand navbar-light">
            <div class="justify-content-between flex-row w-100 navbar-nav">
                <div class="nav-item">
                    <div class="mr-auto navbar-nav">
                        <a href="{{ route('index') }}" class="p-0 nav-link">
                            <img src="{{ asset('images/icons/logo.svg') }}" alt="{{ env('APP_NAME') }}">
                        </a>
                        <a href="#home" class="pl-5 nav-link">
                            Todos os direitos reservado
                        </a>
                    </div>
                </div>
                <div class="social nav-item">
                    <div class="mr-auto navbar-nav">
                        <a href="#home" class="nav-link">
                            <img src="{{ asset('images/icons/facebook.svg') }}" alt="facebook">
                        </a>
                        <a href="#home" class="nav-link">
                            <img src="{{ asset('images/icons/twitter.svg') }}" alt="twitter">
                        </a>
                        <a href="#home" class="nav-link">
                            <img src="{{ asset('images/icons/instagram.svg') }}" alt="instagram">
                        </a>
                        <a href="#home" class="nav-link">
                            <img src="{{ asset('images/icons/youtube.svg') }}" alt="youtube">
                        </a>
                    </div>
                </div>
                <div class="nav-item">
                    <a href="https://www.novam3.com.br" class="nav-link">
                        <strong>Nova M3</strong>
                    </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="d-lg-none container">
        <div class="justify-content-between w-100 nav">
            <div class="nav-item">
                <a href="/home" data-rb-event-key="/home" class="p-0 pt-3 nav-link">
                    <img src="{{ asset('images/icons/logo.svg') }}" alt="{{ env('APP_NAME') }}">
                </a>
            </div>
            <div class="nav-item">
                <div class="mr-auto justify-content-between nav">
                    <a href="#home" class="nav-link">
                        <img src="{{ asset('images/icons/facebook.svg') }}" alt="facebook">
                    </a>
                    <a href="#home" class="nav-link">
                        <img src="{{ asset('images/icons/twitter.svg') }}" alt="twitter">
                    </a>
                    <a href="#home" class="nav-link">
                        <img src="{{ asset('images/icons/instagram.svg') }}" alt="instagram">
                    </a>
                    <a href="#home" class="nav-link">
                        <img src="{{ asset('images/icons/youtube.svg') }}" alt="youtube">
                    </a>
                </div>
            </div>
        </div>
        <div class="mr-auto justify-content-between w-100 pt-2 nav">
            <div class="nav-item">
                <a href="#home" class="pl-0 nav-link">
                    Todos os direitos reservado
                </a>
            </div>
            <div class="nav-item">
                <a href="https://www.novam3.com.br" class="nav-link">
                    <strong>Nova M3</strong>
                </a>
            </div>
        </div>
    </div>
</footer>
