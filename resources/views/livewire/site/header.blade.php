<header>
    <nav class="navbar navbar-light">
        <div class="container">
            <a href="{{ route('index') }}" class="navbar-brand">
                <img src="{{ asset('images/icons/logo.svg') }}" alt="{{ env('APP_NAME') }}">
            </a>
            <div class="justify-content-end flex-row navbar-nav">
                <div class="pr-{{ Auth::check() ? '3' : '4' }} nav-item">
                    @if (Auth::check())
                        <div class="nav-link pb-0">
                            <div class=" avatar pb-0">
                                <div class="btn-group d-block">
                                    <a href="#" class="border-0" title="Opções" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        <img alt="Avatar" class="rounded-circle" width="35px" height="35px"
                                            src="{{ auth()->user()->file ? Storage::url(auth()->user()->file->path) : asset('images/min/avatar-woman.svg') }}">
                                    </a>
                                    <div class="dropdown-menu position-absolute">
                                        <a class="dropdown-item" href="{{ route('postagens.create') }}">Novo Post</a>
                                        <a class="dropdown-item" href="{{ route('home') }}">Minha Conta</a>
                                        <a class="dropdown-item" href="#"
                                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">Sair</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <a href=" {{ route('login') }}" class="nav-link">
                            <img src="{{ asset('images/icons/login.svg') }}" alt="login">
                        </a>
                    @endif
                </div>
                <div class="pr-3 nav-item">
                    <a href="#" class="nav-link active">
                        <img src="{{ asset('images/icons/seach.svg') }}" alt="seach">
                    </a>
                </div>
                <div class="nav-item">
                    <div class="nav-link">
                        <button class="hamburger hamburger--collapse p-0 border-0 navbar-toggler" type="button"
                            data-toggle="collapse" data-target="#basic-navbar-nav" aria-controls="basic-navbar-nav"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="hamburger-box mt-2">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="position-absolute w-100 navbar-collapse collapse" id="basic-navbar-nav">
                <div class="container">
                    <div class="mr-auto w-100 pt-5 pb-5 navbar-nav">
                        @foreach ($categorys as $item)
                            <a href="{{ route('posts', ['slug' => $item->slug]) }}"
                                class="p-3 text-uppercase text-left nav-link">
                                {{ $item->title }}
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="container">
                    <hr>
                    <div class="mr-auto justify-content-between flex-row w-100  pt-2 pb-4 navbar-nav">
                        <a href="{{ route('pages', ['slug' => 'termos-de-uso']) }}"
                            class="p-3 text-uppercase text-center nav-link">
                            termos de uso
                        </a>
                        <a href="{{ route('pages', ['slug' => 'sobre']) }}"
                            class="p-3 text-uppercase text-center nav-link">
                            sobre
                        </a>
                        <a href="{{ route('pages', ['slug' => 'privacidade']) }}"
                            class="p-3 text-uppercase text-center nav-link">
                            privacidade
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>
