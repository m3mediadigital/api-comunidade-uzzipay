<div class="pt-5 pb-5 col-sm-12">
    @forelse ($comments as $item)
        <div class="posts-list row pt-3 pt-lg-1">
            <div class="d-flex justify-md-content-center col-md-1 col-12">
                <div class="MuiAvatar-root MuiAvatar-circle w-100 d-none d-md-block">
                    <img alt="Avatar" width="62px" height="62px"
                        src="{{ asset($item->user->file ? Storage::url($item->user->file->path) : 'images/min/avatar-woman.svg') }}"
                        class="MuiAvatar-img rounded-circle">
                </div>
                <div class="d-md-none row">
                    <div class="col-4">
                        <div class="MuiAvatar-root MuiAvatar-circle w-100">
                            <img alt="Avatar" width="62px" height="62px"
                                src="{{ asset($item->user->file ? Storage::url($item->user->file->path) : 'images/min/avatar-woman.svg') }}"
                                class="MuiAvatar-img rounded-circle">
                        </div>
                    </div>
                    <div class="col">
                        <small class="d-md-none">{{ $item->user->name }}</small>
                        <br>
                        <small class="d-md-none">10min</small>
                    </div>
                </div>
            </div>
            <div class="col-md-10 col-12">
                <small class="d-none d-md-block pt-3">{{ $item->user->name }}</small>
                <small class="d-none d-md-block pt-1">10min
                </small>
                <div class="w-100 posts-card pt-4 pb-4 mt-md-3 border-0 card">
                    <div class="pl-md-5 pr-md-5 pl-2 pr-2 pl-lg-5 pr-lg-5 row">
                        <div class="col-sm-12">
                            <p class="card-text">{{ $item->content }}</p>
                        </div>
                        {{-- <div class="col-sm-12">
                            <div class="flex-row align-items-center likes pt-3 nav card-header-undefined">
                                <div class="d-flex align-content-center pl-0 pl-lg-2 pl-xl-3 nav-item">
                                    <a href="#" class="d-flex p-2 nav-link active">
                                        <div class="icons">
                                            <img src="{{ asset('images/icons/curti-black.svg') }}" alt="curti">
                                        </div>
                                        <span class="pl-2">435</span>
                                    </a>
                                </div>
                                <div class="d-flex align-content-center pl-0 pl-lg-2 pl-xl-3 nav-item">
                                    <a href="#" class="d-flex p-2 nav-link active">
                                        <div class="icons">
                                            <img src="{{ asset('images/icons/2Fcomment-black.svg') }}" alt="curti">
                                        </div>
                                        <span class="pl-2">435</span>
                                    </a>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    @empty

    @endforelse
</div>
