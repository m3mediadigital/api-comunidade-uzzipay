<section class="categorys container border-0 pt-4">
    <nav>
        <div class="nav nav-tabs w-100 d-flex justify-content-sm-between border-0" id="nav-tab" role="tablist">
            <a class="categorys-content-tabs border-0 nav-link pl-2 pr-3 text-uppercase active" id="nav-categorys-tab"
                data-toggle="tab" href="#nav-categorys" role="tab" aria-controls="nav-categorys"
                aria-selected="true">categorias</a>
            <a class="categorys-content-tabs text-center border-0 nav-link pl-2 pr-3 text-uppercase"
                id="nav-recents-tab" data-toggle="tab" href="#nav-recents" role="tab" aria-controls="nav-recents"
                aria-selected="false">recentes</a>
            <a class="categorys-content-tabs text-right border-0 nav-link pl-2 pr-3 text-uppercase" id="nav-betters-tab"
                data-toggle="tab" href="#nav-betters" role="tab" aria-controls="nav-betters"
                aria-selected="false">melhores</a>
        </div>
    </nav>

    <div class="tab-content categorys-content-tabs-content" id="nav-tabContent">
        <div class="tab-pane fade show active p-0" id="nav-categorys" role="tabpanel"
            aria-labelledby="nav-categorys-tab">
            @foreach ($categorys as $item)
                <div class="categorys-content-tabs-content-card w-100 border-0 card">
                    <div class="p-3 pt-0 card-body">
                        <div
                            class="flex-row align-items-center justify-content-between pt-3 pb-3 nav card-header-undefined">
                            <div class="nav-item">
                                <h4><strong>{{ $item->title }}</strong></h4>
                            </div>
                            <div class="nav-item">
                                <a href="{{ route('posts', ['slug' => $item->slug]) }}" class="nav-link">
                                    Ver mais
                                </a>
                            </div>
                        </div>
                        <div class="d-block categorys-card-content pb-3 nav card-header-undefined">
                            @foreach ($item->post as $post)
                                <div class="nav-item">
                                    <a href="{{ route('post', ['slug' => $post->slug]) }}"
                                        class="d-flex p-2 nav-link">
                                        {{ $post->title }}
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="tab-pane fade p-0 posts-list" id="nav-recents" role="tabpanel" aria-labelledby="nav-recents-tab">
            <div class="pt-5">
                @if ($posts->count() >= 1)
                    @livewire('site.posts.card', ['items' => $posts])
                @endif
            </div>
        </div>
        <div class="tab-pane fade p-0" id="nav-betters" role="tabpanel" aria-labelledby="nav-betters-tab">

        </div>
    </div>
</section>
