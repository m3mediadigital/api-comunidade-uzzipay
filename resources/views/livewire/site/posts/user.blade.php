<div class="flex-row align-items-center likes nav card-header-undefined">
    @if (!Route::is('home') || !Route::is('user'))
        <div class="d-flex align-content-center nav-item">
            <a href="/user/{{ $user->slug }}" class="d-flex align-items-center pl-0 pb-2 pr-3 nav-link">
                <div class="d-flex align-items-center avatar">
                    <div style="width: 40px; height: 40px;">
                        <img alt="Avatar" class="rounded-circle" width="35px" width="35px" height="35px"
                            src="{{ $user->file ? Storage::url($user->file->path) : asset('images/min/avatar-woman.svg') }}">
                    </div>
                    <span class="pl-2" style="width: 106px">{{ $user->name }}</span>
                </div>
            </a>
        </div>
    @endif
    <x-Like :post="$post" />
    {{-- <div class="d-flex align-content-center pl-0 pl-lg-2 pl-xl-3 nav-item">
        <a href="/post" data-rb-event-key="/post" class="d-flex p-2 nav-link">
            <div class="icons">
                <img src="{{ asset('images/icons/comment-black.svg') }}" alt="comentario">
            </div>
            <span class="pl-2">98</span>
        </a>
    </div> --}}
    <div class="d-flex align-content-center pl-0 pl-lg-2 nav-item">
        <div class="d-flex p-2 nav-link">
            <div class="icons">
                <img src="{{ asset('images/icons/data.svg') }}" alt="data">
            </div>
            {{-- <span class="pl-2">{{ @$date->format('d M Y') }}</span> --}}
            <span class="pl-2">{{ $date }}</span>
        </div>
    </div>
</div>
