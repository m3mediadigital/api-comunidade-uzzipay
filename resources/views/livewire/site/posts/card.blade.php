@foreach ($items as $item)
    <div class="w-100 posts-card pt-4 pb-4 mb-4 border-0 card">
        <div class="pl-md-4 pr-md-4 pl-2 pr-2 pl-lg-4 pr-lg-4 row">
            <div class="col-xl-7 col-lg-6 col-sm-12">
                <div class="card-title h5">
                    <a href="{{ route('post', ['slug' => $item->slug]) }}">
                        {{ $item->title }}
                    </a>
                </div>
                <div class="card-text text-sm">
                    {!! Str::limit($item->content, 250) !!}
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 col-sm-12 d-flex align-items-center justify-lg-content-end pt-4 pt-lg-0">
                @livewire('site.posts.user', ['user' => $item->user, 'date' => $item->updated_at, 'post' => $item],
                key($item->id))
            </div>
        </div>
    </div>
@endforeach
