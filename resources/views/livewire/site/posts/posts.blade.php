<section class="posts">
    <div class="posts-list container">
        <div class="pt-4 pl-2 pl-md-5 pb-4">
            <h3><strong>{{ $item->title }}</strong></h3>
        </div>
        @livewire('site.posts.card', ['items' => $item->post], key($item->id))
    </div>
</section>
