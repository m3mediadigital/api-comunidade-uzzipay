<div class="table-responsive">
    <table class="table align-items-center table-flush">
        <thead class="thead-light">
            <tr>
                <th scope="col">Título</th>
                <th scope="col">Categoria</th>
                <th scope="col">Curtidas</th>
                <th scope="col">Comentarios</th>
                <th scope="col">Ativo</th>
                <th scope="col">Criado</th>
                <th scope="col">Atualizado</th>
                <th scope="col">Opções</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts->post as $item)
                <tr>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->category->title }}</td>
                    <td>{{ $item->like->count() }}</td>
                    <td>{{ $item->comment->count() }}</td>
                    <td>
                        <span class="badge badge-pill badge-{{ $item->active == 1 ? 'success' : 'danger' }}">
                            {{ $item->active == 1 ? 'Ativo' : 'Inativo' }}
                        </span>
                    </td>
                    <td>{{ $item->created_at->format('d-m-Y') }}</td>
                    <td>{{ $item->updated_at->format('d-m-Y') }}</td>
                    <td>
                        <div class="dropdown">
                            <a class="btn btn-sm text-black-50" href="#" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-list"></i> Opções
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a class="dropdown-item" href="{{ route('categories.edit', $item) }}">
                                    <i class="fas fa-edit"></i>Editar</a>
                                <a class="dropdown-item" href="{{ route('categories.show', $item) }}">
                                    <i class="fas fa-heart"></i>Curtidas</a>
                                <a class="dropdown-item" href="{{ route('categories.show', $item) }}">
                                    <i class="far fa-comments"></i>Comentários</a>
                                @livewire('delete', ['route' => route('posts.destroy', $item)],
                                key($item->id))
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="card-footer py-4">
    {{ $posts->post()->paginate(1)->links() }}
</div>
