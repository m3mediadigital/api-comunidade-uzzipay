<div class="card w-100">
    <!-- Card header -->
    <div class="card-header border-0">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <h3 class="mb-0">Lista</h3>
            </div>
            <div class="col-sm-12 col-md-4">
                <div id="datatable-basic_filter" class="dataTables_filter">
                    <input wire:model="search" type="search" class="form-control form-control-sm"
                        placeholder="Pesquisar">
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
                <tr>
                    <th scope="col">User</th>
                    <th scope="col">Redes</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Posts</th>
                    <th scope="col">Criado em</th>
                    <th scope="col">Atualizado em</th>
                    <th scope="col">Opções</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $item)
                    <tr>
                        <th scope="row">
                            <div class="media align-items-center">
                                <div class="avatar rounded-circle mr-3">
                                    <img alt="Image placeholder"
                                        src="{{ asset($item->file ? $item->file->path : 'images/min/avatar-woman.svg') }}">
                                </div>
                                <div class="media-body">
                                    <span class="name mb-0 text-sm">{{ $item->name }}</span><br>
                                    <span class="mb-0 text-sm">CPF: {{ $item->cpf }}</span>
                                </div>
                            </div>
                        </th>
                        <td>
                            <div class="media-body">
                                <a href="{{ $item->instagram }}" target="_blank" rel="noopener noreferrer">
                                    Instagram
                                </a>
                                <br>
                                <a href="{{ $item->facebook }}" target="_blank" rel="noopener noreferrer">
                                    Facebook
                                </a>
                            </div>
                        </td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->post->count() }}</td>
                        <td>{{ $item->created_at->format('d-m-Y') }}</td>
                        <td>{{ $item->updated_at->format('d-m-Y') }}</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-sm text-black-50" href="#" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-list"></i> Opções
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item" href="{{ route('users.show', $item) }}">
                                        <i class="fas fa-mail-bulk"></i>Posts</a>
                                    <a class="dropdown-item" href="{{ route('users.edit', $item) }}">
                                        <i class="fas fa-edit"></i>Editar</a>
                                    @livewire('delete', ['route' => route('users.destroy', $item)],
                                    key($item->id))
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer py-4">
        {{ $users->links() }}
    </div>
</div>
