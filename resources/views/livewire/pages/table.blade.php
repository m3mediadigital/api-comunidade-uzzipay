<div class="card w-100">
    <!-- Card header -->
    <div class="card-header border-0">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <h3 class="mb-0">Lista</h3>
            </div>
            {{-- <div class="col-sm-12 col-md-4">
                <div id="datatable-basic_filter" class="dataTables_filter">
                    <input wire:model="search" type="search" class="form-control form-control-sm"
                        placeholder="Pesquisar">
                </div>
            </div> --}}
        </div>
    </div>
    <div class="table-responsive">
        <table class="table align-items-center table-flush">
            <thead class="thead-light">
                <tr>
                    <th scope="col">Título</th>
                    <th scope="col">Slug</th>
                    <th scope="col">Criado</th>
                    <th scope="col">Atualizado</th>
                    <th scope="col">Opções</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pages as $item)
                    <tr>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->slug }}</td>
                        <td>{{ $item->created_at->format('d-m-Y') }}</td>
                        <td>{{ $item->updated_at->format('d-m-Y') }}</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-sm text-black-50" href="#" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-list"></i> Opções
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <a class="dropdown-item" href="{{ route('pages.edit', $item) }}">
                                        <i class="fas fa-edit"></i>Editar</a>
                                    {{-- @livewire('delete', ['route' => route('pages.destroy', $item)],
                                    key($item->id)) --}}
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="card-footer py-4">
        {{ $pages->links() }}
    </div>
</div>
