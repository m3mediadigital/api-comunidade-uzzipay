<meta name="description" content="{{ @$description ?? 'Bem vindo a nossa comunidade' }}">
<meta name="keywords" content="">
<meta name="robots" content="index,follow,noodp">
<meta name="googlebot" content="index,follow">
<meta name="subjet" content="{{ @$subject ?? env('APP_NAME') }}">
<meta name="abstract" content="{{ @$subject ?? env('APP_NAME') }}">
<meta name="topic" content="{{ @$subject ?? env('APP_NAME') }}">
<meta name="summary" content="{{ @$subject ?? env('APP_NAME') }}">
<meta property="og:url" content="{{ request()->fullUrl() }}">
<meta property="og:type" content="{{ env('OG_TYPE', 'website') }}">
<meta property="og:title" content="{{ @$title ?? env('APP_NAME') }}">
<meta property="og:image" content="{{ @$file ?? asset('images/icons/logo.svg') }}">
<meta property="og:description" content="{{ @$description ?? 'Bem vindo a nossa comunidade' }}">
<meta property="og:site_name" content="{{ @$title ?? env('APP_NAME') }}">
<meta property="og:locale" content="pt_BR">
<meta itemprop="name" content="{{ @$title ?? env('APP_NAME') }}">
<meta itemprop="description" content="{{ @$description ?? 'Bem vindo a nossa comunidade' }}">
<meta itemprop="image" content="{{ @$file ?? asset('images/icons/logo.svg') }}">
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="{{ request()->fullUrl() }}">
<meta name="twitter:url" content="{{ request()->fullUrl() }}">
<meta name="twitter:title" content="{{ @$title ?? env('APP_NAME') }}">
<meta name="twitter:description" content="{{ @$description ?? 'Bem vindo a nossa comunidade' }}">
<meta name="twitter:image" content="{{ @$file ?? asset('images/icons/logo.svg') }}">
