<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="https://uzzipay.com/images/favicon.png" type="image/png">
        {{ @$meta }}

        <title> {{ env('APP_NAME') . ' - ' . @$title }}</title>
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        <link rel="author" href="Felipe Mendonça">
        <link rel="me" href="feliipemendonca@outlook.com" type="text/html">
        <link rel="me" href="mailto:feliipemendonca@outlook.com">
        <link rel="me" href="sms:+5584991956348">
        <script src="{{ mix('js/app.js') }}"></script>

        {{ @$css }}
    </head>

    <body>
        @auth()
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>            
        @endauth
        
        @livewire('site.header')
        {{ @$content }}
        @livewire('site.footer')

        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

        <script>
            @if(session('success'))
                Swal.fire(
                    'Sucesso',
                    '{!! session('success') !!}',
                    'success'   
                )
            @endif

            @if(session('error'))
                Swal.fire(
                    'Erro!',
                    '{!! session('error') !!}',
                    'error',
                )
            @endif
            @if (session('status'))
                Swal.fire(
                    'Atenção',
                    '{!! session('status') !!}',
                    'info',
                )
            @endif
        </script>
        {{ @$js }}
    </body>
</html>
