<div class="d-flex align-content-center pl-0 pl-lg-2 pl-xl-3 nav-item">
    @if (auth()->user())
        <button type="button" id="{{ $post->id }}" class="d-flex p-2 nav-link border-0"
            style="background: transparent; outline: none;">
            <div class="icons">
                @if ($post
        ->like()
        ->where('user_id', auth()->user()->id)
        ->first())
                    <img id="image{{ $post->id }}" width="17px" src="{{ asset('images/icons/gostar.svg') }}"
                        alt="curti">
                @else
                    <img id="image{{ $post->id }}" width="17px" src="{{ asset('images/icons/curti-black.svg') }}"
                        alt="curti">
                @endif

            </div>
            <span class="pl-2" id="curti{{ $post->id }}">{{ $post->like->count() }}</span>
        </button>
    @else
        <a href="{{ route('login') }}" id="{{ $post->id }}" class="d-flex p-2 nav-link border-0"
            style="background: transparent">
            <div class="icons">
                <img id="image{{ $post->id }}" width="17px" src="{{ asset('images/icons/curti-black.svg') }}"
                    alt="curti">
            </div>
            <span class="pl-2" id="curti{{ $post->id }}">{{ $post->like->count() }}</span>
        </a>
    @endif
</div>

@auth
    <script type="text/javascript">
        $('#{{ $post->id }}').click(() => {
            $.ajax({
                type: 'GET',
                url: '{{ route('curti', ['id' => $post->id]) }}',

                success: (data) => {
                    if (data.status === true) {
                        $('#curti{{ $post->id }}').html(data.count)
                        $('#image{{ $post->id }}').attr('src', '/images/icons/gostar.svg')
                    }

                    if (data.status === false) {
                        $('#curti{{ $post->id }}').html(data.count)
                        $('#image{{ $post->id }}').attr('src', '/images/icons/curti-black.svg')
                    }
                }
            })
        })

    </script>
@endauth
