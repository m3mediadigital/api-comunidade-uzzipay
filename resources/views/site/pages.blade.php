<x-site>
    <x-slot name="meta">
        <x-Metas :title="$page->title" :file="null" :description="$page->content" :subject="$page->title" />
    </x-slot>
    <x-slot name="title">
        {{ __($page->title) }}
    </x-slot>
    <x-slot name="content">
        <section class="posts">
            <div class="posts-intern container">
                <div class="pt-5 pb-4">
                    <h3><strong>{{ $page->title }}</strong></h3>
                </div>

                <div class="hr pt-4 pb-4">
                    <hr>
                </div>
                <div class="posts-intern-text pt-5">
                    {!! $page->content !!}
                </div>

                <div class="row justify-content-end">
                    <div class="hr pt-4 pb-4">
                        <hr>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
</x-site>
