<x-site>
    <x-slot name="meta">
        <x-Metas :title="null" :file="null" :description="null" :subject="null" />
    </x-slot>
    <x-slot name="title">
        {{ __('Bem Vindo') }}
    </x-slot>

    <x-slot name="content">

        <section class="posts">
            <div class="d-none d-lg-block container">
                <div class="row">
                    @if ($featured->count() >= 1)
                        <div class="col-lg-6 col-sm-12">
                            <a href="{{ route('post', ['slug' => $featured[0]->post->slug]) }}"
                                class=" text-white text-decoration-none">
                                <div style="background-image:url({{ $featured[0]->post->file ? $featured[0]->post->file->path : asset('images/postOne.png') }})"
                                    class="w-100 border-0 posts-card posts-card-one card">
                                    <div class="pt-5 w-100 card-body">
                                        <div class="card-body-content position-absolute">
                                            <div class="w-75 card-title h5">
                                                <strong>{{ $featured[0]->post->title }}</strong>
                                            </div>
                                            <p class="card-text">{!! Str::limit($featured[0]->post->content, 250) !!}</p>
                                            @livewire('site.posts.user',['user'=>$featured[0]->post->user,'post'=>$featured[0]->post],key($featured[0]->id))
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endif

                    <div class="col-lg-6 col-sm-12">
                        <div class="row">
                            @if ($featured->count() >= 1 && isset($featured[1]))
                                <div class="col-12">
                                    <a href="{{ route('post', ['slug' => $featured[0]->post->slug]) }}"
                                        class=" text-white text-decoration-none">
                                        <div style="background-image:url({{ $featured[1]->post->file ? $featured[1]->post->file->path : asset('images/postTwo.jpg') }})"
                                            class="w-100 border-0 posts-card posts-card-two mb-3 card">
                                            <div class="pt-5 w-100 card-body">
                                                <div class="card-body-content position-absolute">
                                                    <div class="w-75 card-title h5">
                                                        <strong>{{ $featured[1]->post->title }}</strong>
                                                    </div>
                                                    @livewire('site.posts.user',['user'=>$featured[1]->post->user,'post'=>$featured[1]->post],key($featured[1]->id))
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                            @if ($featured->count() >= 1 && isset($featured[2]))
                                <div class="col-12">
                                    <a href="{{ route('post', ['slug' => $featured[0]->post->slug]) }}"
                                        class=" text-white text-decoration-none">
                                        <div style="background-image:url({{ $featured[2]->post->file ? $featured[2]->post->file->path : asset('images/postTwo.jpg') }})"
                                            class="w-100 border-0 posts-card posts-card-two mb-3 card">
                                            <div class="pt-5 w-100 card-body">
                                                <div class="card-body-content position-absolute">
                                                    <div class="w-75 card-title h5">
                                                        <strong>{{ $featured[2]->post->title }}</strong>
                                                    </div>
                                                    @livewire('site.posts.user',['user'=>$featured[2]->post->user,'post'=>$featured[2]->post],key($featured[2]->post->id))
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div id="carouselExampleIndicators" class="carousel slide d-lg-none" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    @foreach ($featured as $item)
                        <div interval="4000" class="{{ $loop->first ? 'active ' : '' }}carousel-item">
                            <div style="background-image:url({{ $item->post->file ? $item->post->file->path : asset('images/postOne.png') }})"
                                class="w-100 border-0 posts-card posts-card-one card">
                                <div class="pt-5 w-100 card-body">
                                    <div class="card-body-content position-absolute">
                                        <div class="w-75 card-title h5">
                                            <strong>{{ $item->post->title }}</strong>
                                        </div>
                                        <p class="card-text">{!! Str::limit($item->post->content, 250) !!}</p>
                                        @livewire('site.posts.user',['user'=>$item->post->user,'post'=>$item->post],key($item->id))
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </section>

        @livewire('site.categorys')
    </x-slot>
</x-site>
