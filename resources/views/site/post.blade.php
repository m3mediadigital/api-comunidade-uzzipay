<x-site>
    <x-slot name="meta">
        @php
            $file = $post->hasFile ? $post->hasFile->file->path : null;
        @endphp
        <x-Metas :title="$post->title" :file="$file" :description="$post->content" :subject="$post->title" />
    </x-slot>
    <x-slot name="title">
        {{ __($post->title) }}
    </x-slot>
    <x-slot name="content">
        <section class="posts">
            <div class="posts-intern container">
                <div class="pt-5 pb-4">
                    <h3><strong>{{ $post->title }}</strong></h3>
                </div>

                @livewire('site.posts.user', ['user' => $post->user, 'date' => $post->updated_at, 'post' => $post],
                key($post->id))

                <div class="hr pt-4 pb-4">
                    <hr>
                </div>

                <figure class="figure">
                    <img src="{{ $post->hasFile ? $post->hasFile->file->path : asset('images/post.jpg') }}"
                        class="img-fluid w-100" alt="{{ $post->title }}">
                    <figcaption class="figure-caption pt-1">
                        <i>{{ $post->title }}</i>
                    </figcaption>
                </figure>
                <div class="posts-intern-text pt-5">
                    {!! $post->content !!}
                </div>
                @isset($post->link)
                    <div class="pt-5 pb-5">
                        <iframe class="w-100" height="500" src="https://www.youtube.com/embed/{{ $post->link }}"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </div>
                @endisset

                <div class="hr pt-4 pb-4">
                    <hr>
                </div>
                <div class="posts-intern-comments">
                    <p class="pb-3">Comentários</p>
                    <div class="row">
                        <div class="d-flex justify-content-center col-md-1 col-sm-2 col-2 w-100">
                            <div class="rounded-circle" style="width: 62px; height:62px">
                                <img alt="Avatar" class="rounded-circle" width="62px" height="62px"
                                    src="{{ auth()->user()->file ? Storage::url(auth()->user()->file->path) : asset('images/min/avatar-woman.svg') }}">
                            </div>
                        </div>
                        <div class="col-md-11 col-sm-10 col-10">
                            <form class="pb-5" method="post" action="{{ route('comments') }}">
                                @csrf
                                @method('post')
                                <input type="hidden" name="post_id" value="{{ $post->id }}">
                                <textarea rows="5" class="rounded-0 form-control"
                                    name="content">{{ old('content') }}</textarea>
                                <button type="submit"
                                    class="border-0 position-absolute pt-2 pb-2 pr-5 pl-5 rounded-0 btn btn-primary">
                                    Comentar
                                </button>
                            </form>
                        </div>
                        @livewire('site.comment', ['post' => $post->id], key($post->id))
                    </div>
                </div>

                @if ($post->category
        ->post()
        ->where('active', 1)
        ->count() >= 1)

                    <div class="hr pt-4 pb-4">
                        <hr>
                    </div>

                    <div class="posts">
                        <div class="posts-list">
                            <p class="pb-3">Veja também</p>
                            @livewire('site.posts.card', ['items' => $post->category->post()->where('active',1)->get()],
                            key($post->id))
                        </div>
                    </div>
                @endif
            </div>
        </section>
    </x-slot>
</x-site>
