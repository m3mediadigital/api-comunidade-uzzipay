<x-site>
    <x-slot name="meta">
        <x-Metas :title="$category->title" :file="null" :description="$category->title" :subject="$category->title" />
    </x-slot>
    <x-slot name="title">
        {{ __($category->title) }}
    </x-slot>

    <x-slot name="content">
        @livewire('site.posts.posts',['posts' => $category])
    </x-slot>
</x-site>
