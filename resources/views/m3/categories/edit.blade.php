<x-layout.app>

    <x-slot name="content">

        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">Editar Categoria</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body border-0">
                            <form id="formCategoryEdit" action="{{ route('categories.update', $category) }}"
                                method="post">
                                @csrf
                                @method('put')
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Nome da categoria" name="title"
                                        id="title" value="{{ $category->title }}">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Salvar</button>
                                    <a href="{{ route('categories.index') }}" class="btn btn-secondary">Voltar</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-layout.app>
