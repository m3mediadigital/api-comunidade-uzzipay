<x-layout.app>
    <x-slot name="title">
        {{ __('Categorias') }}
    </x-slot>

    <x-slot name="content">
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        @livewire('categorys.lists')
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-layout.app>
