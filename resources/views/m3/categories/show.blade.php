<x-layout.app>
    <x-slot name="title">
        {{ __('Categorias / ' . $category->title) }}
    </x-slot>

    <x-slot name="content">
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        @livewire('categorys.posts',['categorys' => $category])
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-layout.app>
