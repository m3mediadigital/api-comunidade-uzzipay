<x-layout.app>
    <x-slot name="title">
        {{ __('Usuários / ' . $user->name) }}
    </x-slot>

    <x-slot name="content">
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">Postagens</h3>
                                </div>
                                <div class="col-4 text-right">
                                    <input wire:model="search" class="form-control" type="text" placeholder="Pesquisar">
                                </div>
                                <div class="col-12">
                                    <div class="collapse pt-5{{ old('title') ? ' show' : '' }}" id="collapseExample">
                                        <form class="" method="post" action="{{ route('categories.store') }}">
                                            @csrf
                                            <div class="form-row">
                                                <div class="form-group col-12 col-md-12 col-lg-10 col-xl-11">
                                                    <input type="text" class="form-control" id="inlineFormInputName2"
                                                        name="title" placeholder="Categoria"
                                                        value="{{ old('title') }}">
                                                </div>
                                                <div class="form-group col-12 col-md-12 col-lg-2 col-xl-1">
                                                    <button type="submit"
                                                        class="btn btn-success mb-2 w-auto">Salvar</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @livewire('users.posts',['posts' => $user])
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-layout.app>
