<x-layout.app>
    <x-slot name="title">
        {{ __('Usuários / ' . $user->name) }}
    </x-slot>

    <x-slot name="content">
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col-12 col-md-8">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">Informações</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{ route('users.update', $user) }}" autocomplete="off">
                                @csrf
                                @method('put')

                                <div class="form-group">
                                    <label class="form-control-label" for="input-name">{{ __('Name') }}</label>
                                    <input type="text" name="name"
                                        class="form-control form-control-alternative @error('name') is-invalid @enderror"
                                        placeholder="{{ __('Name') }}" value="{{ $user->name }}" required>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label" for="input-name">{{ __('CPF') }}</label>
                                    <input type="text" name="cpf"
                                        class="form-control form-control-alternative @error('cpf') is-invalid @enderror"
                                        placeholder="{{ __('000.000.000-00') }}" value="{{ $user->cpf }}">

                                    @error('cpf')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-email">{{ __('Email') }}</label>
                                    <input type="email" name="email" id="input-email"
                                        class="form-control form-control-alternative @error('email') is-invalid @enderror"
                                        placeholder="{{ __('Email') }}" value="{{ $user->email }}" required>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="col-12 col-md-6">
                                            <label class="form-control-label"
                                                for="input-email">{{ __('Facebook') }}</label>
                                            <input type="url" name="facebook" id="input-email"
                                                class="form-control form-control-alternative @error('facebook') is-invalid @enderror"
                                                placeholder="{{ __('Facebook') }}" value="{{ $user->facebook }}">

                                            @error('facebook')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label class="form-control-label"
                                                for="input-email">{{ __('Instagram') }}</label>
                                            <input type="url" name="instagram" id="input-email"
                                                class="form-control form-control-alternative @error('instagram') is-invalid @enderror"
                                                placeholder="{{ __('Instagram') }}"
                                                value="{{ $user->instagram }}">

                                            @error('instagram')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Atualizar') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-4">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">Senha</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form method="post" action="{{ route('profile.password') }}" autocomplete="off">
                                @csrf
                                @method('put')
                                <div class="form-group">
                                    <label class="form-control-label"
                                        for="input-password">{{ __('Nova Senha') }}</label>
                                    <input type="password" name="password" id="input-password"
                                        class="form-control form-control-alternative @error('password') is-invalid @enderror"
                                        placeholder="{{ __('Nova Senha') }}" value="" required>

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label"
                                        for="input-password">{{ __('Confirme Senha') }}</label>
                                    <input type="password" name="password_confirmation" id="input-password"
                                        class="form-control form-control-alternative @error('password_confirmation') is-invalid @enderror"
                                        placeholder="{{ __('Confirme Senha') }}" value="" required>

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="text-center">
                                    <button type="submit"
                                        class="btn btn-success mt-4">{{ __('Change password') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-layout.app>
