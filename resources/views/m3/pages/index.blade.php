<x-layout.app>
    <x-slot name="title">
        {{ __('Páginas Estáticas') }}
    </x-slot>


    <x-slot name="content">
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        @livewire('pages.table')
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-layout.app>
