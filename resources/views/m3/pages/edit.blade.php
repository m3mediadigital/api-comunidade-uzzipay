<x-layout.app>

    <x-slot name="title">
        {{ __('Pàginas Estáticas / ' . $page->title) }}
    </x-slot>

    <x-slot name="content">
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">Editar</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('pages.update', $page) }}" method="post">
                                @csrf
                                @method('put')

                                <div class="form-group">
                                    <label for="title">Título</label>
                                    <input type="text" name="title" class="form-control"
                                        value="{{ $page->title ?? old('title') }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Conteúdo</label>
                                    <textarea name="cotnent" id="content">{{ $page->content }}</textarea>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Atualizar') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>

    <x-slot name="js">
        <script src="https://cdn.ckeditor.com/4.16.0/basic/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('content', {
                height: 350
            });

        </script>
    </x-slot>

</x-layout.app>
