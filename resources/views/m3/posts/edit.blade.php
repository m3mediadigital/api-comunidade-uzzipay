<x-layout.app>

    <x-slot name="title">
        {{ __('Postagens / ' . $post->user->name . ' / ' . $post->title) }}
    </x-slot>

    <x-slot name="content">
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col col-12 col-xl-8">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">Editar</h3>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('posts.update', $post) }}" method="post">
                                @csrf
                                @method('put')
                                <input type="hidden" name="user_id" value="{{ $post->user_id }}">
                                <div class="form-row">
                                    <div class="col-12 col-xl-6">
                                        <div class="form-group">
                                            <label for="title">Título</label>
                                            <input type="text" name="title" class="form-control"
                                                value="{{ $post->title ?? old('title') }}">
                                        </div>
                                    </div>
                                    <div class="col-12 col-xl-6">
                                        <div class="form-group">
                                            <label for="title">Destaque do site?</label>
                                            <select name="featured_post" class="form-control">
                                                <option value="">Selecione</option>
                                                <option value="1" {{ $post->featured == 1 ? 'selected' : '' }}>Ativo
                                                </option>
                                                <option value="0" {{ $post->featured == 0 ? 'selected' : '' }}>Inativo
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-xl-6">
                                        <div class="form-group">
                                            <label for="title">Categorias</label>
                                            <select name="category_id" class="form-control">
                                                <option value="">Selecione</option>
                                                @foreach ($categorys as $item)
                                                    <option value="{{ $item->id }}"
                                                        {{ $post->category->id === $item->id ? ' selected' : '' }}>
                                                        {{ $item->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-xl-6">
                                        <div class="form-group">
                                            <label for="title">Ativo</label>
                                            <select name="active" class="form-control">
                                                <option value="">Selecione</option>
                                                <option value="1" {{ $post->active == 1 ? 'selected' : '' }}>Ativo
                                                </option>
                                                <option value="0" {{ $post->active == 0 ? 'selected' : '' }}>Inativo
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <label for="">Conteúdo</label>
                                        <textarea name="cotnent" id="content">{{ $post->content }}</textarea>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-success mt-4">{{ __('Atualizar') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-4">
                    <div class="card shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h3 class="mb-0">Imagem</h3>
                                </div>
                                <div class="col-4 text-right">
                                    <button class="btn btn-danger btn-sm" type="button" data-toggle="collapse"
                                        data-target="#collapseExample" aria-expanded="false"
                                        aria-controls="collapseExample">
                                        Excluir
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFileLang" name="file">
                                <label class="custom-file-label" for="customFileLang">Select file</label>
                            </div>
                            <img id="imagem" class="img-fluid mt-3 text-center" src="" />
                        </div>
                    </div>
                </div>
            </div>
    </x-slot>

    <x-slot name="js">
        <script src="https://cdn.ckeditor.com/4.16.0/basic/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('content', {
                height: 150
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader()
                    reader.onload = function(e) {
                        $('#imagem').attr('src', e.target.result)
                    }
                    reader.readAsDataURL(input.files[0])
                }
            }
            $('#customFileLang').change(function() {
                readURL(this)
            })

        </script>
    </x-slot>

</x-layout.app>
