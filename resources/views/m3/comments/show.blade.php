<x-layout.app>
    <x-slot name="title">
        {{ __('Postagens / ' . $comment->post->title . ' / Comentário') }}
    </x-slot>
    <x-slot name="content">
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        <div class="card w-100">
                            <!-- Card header -->
                            <div class="card-header border-0">
                                <div class="row">
                                    <div class="col-sm-12 col-md-8">
                                        <h3 class="mb-0">Comentários</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body border-0">
                                <form action="{{ route('comentarios.update', $comment->id) }}" method="post">
                                    @csrf
                                    @method('put')

                                    <div class="form-group">
                                        <label for="title">Ativo</label>
                                        <select name="active" class="form-control">
                                            <option value="">Selecione</option>
                                            <option value="1" {{ $comment->active == 1 ? 'selected' : '' }}>Ativo
                                            </option>
                                            <option value="0" {{ $comment->active == 0 ? 'selected' : '' }}>Inativo
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Comentário</label>
                                        <textarea name="content" class="form-control" id="" cols="30"
                                            rows="7">{{ $comment->content }}</textarea>
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-success rounded-0">Atualizar</button>
                                    </div>
                                </form>
                                <hr>
                                <form action="{{ route('comentarios.destroy', $comment->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <div class="form-group">
                                        <label for="">Excluir Comentário? Informe o motivo para o usuário!</label>
                                        <textarea name="message" class="form-control" required rows="7"></textarea>
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-danger rounded-0">Excluir</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-layout.app>
