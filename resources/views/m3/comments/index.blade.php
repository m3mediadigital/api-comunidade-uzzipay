<x-layout.app>
    <x-slot name="title">
        {{ __('Postagens / ' . $post->title . ' / Comentários') }}
    </x-slot>


    <x-slot name="content">
        <div class="container-fluid mt--7">
            <div class="row">
                <div class="col">
                    <div class="card shadow">
                        @livewire('comment.lists',['post' => $post->id])
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
</x-layout.app>
